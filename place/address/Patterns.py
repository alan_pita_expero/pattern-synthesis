class GooglePlacesLookup:
    def project(self,constraint,args,target):
        googlePlacesId  = constraint.getArgument('googlePlacesId',target)
        name  = constraint.getArgument('name',target)
        city   = constraint.getArgument('city',target)
        state  = constraint.getArgument('state',target)
        country  = constraint.getArgument('country',target)
        address  = constraint.getArgument('address',target)
        lat  = constraint.getArgument('lat',target)
        lng  = constraint.getArgument('lng',target)

        useCity = (city.isSingle() and state.isSingle() and country.isSingle())
        reduceCount = 0
        if ('name' in args or 'address' in args or 'city' in args or 'state' in args or 'country' in args) and (name.isSingle() or address.isSingle() or useCity) and googlePlacesId.isUniversal():
            query = dict()
            queryTerms = []
            if name.isSingle():
                queryTerms.append(name.single())
            else:
                queryTerms.append('businesses')
            if address.isSingle():
                queryTerms.append('at')
                queryTerms.append(address.single())
            if useCity:
                queryTerms.append('near')
                queryTerms.append(city.single())
                if state.single() != '':
                    queryTerms.append(state.single())
                queryTerms.append(country.single())
            query['query'] = ' '.join(queryTerms)
            query['key'] = packages.os.environ.get('GOOGLE_API_KEY')
            url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?%s' % packages.urllib.urlencode(query)
            response = packages.requests.get(url)
            placeIds = [ candidate.get('place_id') for candidate in response.json().get('results') ]
            constraint.setArgument('googlePlacesId',target,packages.values.Values(placeIds))
            reduceCount += 1
        if 'googlePlacesId' in args and googlePlacesId.isSingle():
            if not name.isSingle() or not address.isSingle():
                query = dict()
                query['placeid'] = googlePlacesId.single()
                query['key'] = packages.os.environ.get('GOOGLE_API_KEY')
                url = 'https://maps.googleapis.com/maps/api/place/details/json?%s' % packages.urllib.urlencode(query)
                print 'DEBUG: url = %s' %url
                response = packages.requests.get(url)
                constraint.setArgument('name',target,response.json().get('result').get('name'))
                constraint.setArgument('address',target,response.json().get('result').get('formatted_address'))
                constraint.setArgument('lat',target,response.json().get('result').get('geometry').get('location').get('lat'))
                constraint.setArgument('lng',target,response.json().get('result').get('geometry').get('location').get('lng'))
                reduceCount += 1
        if reduceCount > 0:
            return packages.constraint.REDUCE
        else:
            return packages.constraint.CONTINUE


class AddressFromGooglePlaces(packages.patterns.Object):
    global GooglePlacesLookup

    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'googlePlacesId': { 'name': 'googlePlacesId', 'encoding': { 'java': 'java.lang.String' } },
            'name': { 'name': 'name', 'encoding': { 'java': 'java.lang.String' } },
            'city': { 'name': 'city', 'semantic': 'place.city' },
            'address': { 'name': 'address', 'encoding': { 'java': 'java.lang.String' } },
            'lat': { 'name': 'lat', 'encoding': { 'java': 'java.lang.Float' } },
            'lng': { 'name': 'lng', 'encoding': { 'java': 'java.lang.Float' } },
        })

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.constrain(GooglePlacesLookup(),dict(
            googlePlacesId=binding.rootPath.get('googlePlacesId'),
            name=binding.rootPath.get('name'),
            city=binding.rootPath.get('city.city'),
            state=binding.rootPath.get('city.state'),
            country=binding.rootPath.get('city.country'),
            address=binding.rootPath.get('address'),
            lat=binding.rootPath.get('lat'),
            lng=binding.rootPath.get('lng')
        ))

exports = [AddressFromGooglePlaces]
