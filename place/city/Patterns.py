# -*- coding: utf-8 -*-
import json

# JSON source for world cities is taken from:
# https://github.com/lutangar/cities.json.git
fp = open('%sUS-states-and-cities.json' % (scriptPath))
byState = json.load(fp)

values = []
for state in byState.keys():
    for city in byState.get(state):
        values.append(dict(state=state,city=city,country='us'))

print 'DEBUG: There are %d values for %s' % (len(values),scriptPackage)
class Cities(packages.patterns.Trait):
    global values

    def __init__(self):
        packages.patterns.Trait.__init__(self,scriptPackage,values,{
            'country': { 'java': 'java.lang.String' },
            'state': { 'java': 'java.lang.String' },
            'city': { 'java': 'java.lang.String' },
        })

exports = [Cities]
