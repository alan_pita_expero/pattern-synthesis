fp = open('%s/iso3166.json' % (scriptPath))
db = packages.json.load(fp)

class Country(packages.patterns.Trait):
    global db

    def __init__(self):
        packages.patterns.Trait.__init__(self,scriptPackage,db,{
            "name": { 'java': 'java.lang.String' },
            "alpha-2": { 'java': 'java.lang.String' },
            "alpha-3": { 'java': 'java.lang.String' },
            "country-code": { 'java': 'java.lang.String' },
            "iso_3166-2": { 'java': 'java.lang.String' },
            "region": { 'java': 'java.lang.String' },
            "sub-region": { 'java': 'java.lang.String' },
            "intermediate-region": { 'java': 'java.lang.String' },
            "region-code": { 'java': 'java.lang.String' },
            "sub-region-code": { 'java': 'java.lang.String' },
            "intermediate-region-code": { 'java': 'java.lang.String' }
        })

exports = [Country]
