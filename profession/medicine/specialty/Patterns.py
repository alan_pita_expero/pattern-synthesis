# -*- coding: utf-8 -*-
import json

# JSON source for healthcare specializations is taken from:
# https://github.com/Preferral/Health-Care-Provider-Taxonomy-JSON.git
def flatten(hier):
    result = dict()
    for propName in ['name','description','taxonomy']:
        if hier.get(propName):
            result[propName] = hier.get(propName)
    return result

fp = open('%staxonomy_140.json' % (scriptPath))
taxonomy = json.load(fp)

values = []
for category in taxonomy.get('categories'):
    for type in category.get('types') or [dict()]:
        for classification in type.get('classifications') or [dict()]:
            for specialization in classification.get('specializations') or [dict()]:
                values.append(dict(
                    category=flatten(category),
                    type=flatten(type),
                    classification=flatten(classification),
                    specialization=flatten(specialization)
                ))

print 'DEBUG: There are %d values for %s' % (len(values),scriptPackage)
class MedicalSpecialties(packages.patterns.Trait):
    global values

    def __init__(self):
        packages.patterns.Trait.__init__(self,scriptPackage,values,{
            'category': { 'properties': {
                'name': { 'java': 'java.lang.String' },
                'description': { 'java': 'java.lang.String' },
                'taxonomy': { 'java': 'java.lang.String' }
            } },
            'type': { 'properties': {
                'name': { 'java': 'java.lang.String' },
                'description': { 'java': 'java.lang.String' },
                'taxonomy': { 'java': 'java.lang.String' }
            } },
            'classification': { 'properties': {
                'name': { 'java': 'java.lang.String' },
                'description': { 'java': 'java.lang.String' },
                'taxonomy': { 'java': 'java.lang.String' }
            } },
            'specialization': { 'properties': {
                'name': { 'java': 'java.lang.String' },
                'description': { 'java': 'java.lang.String' },
                'taxonomy': { 'java': 'java.lang.String' }
            } },
        })

exports = [MedicalSpecialties]
