# -*- coding: utf-8 -*-
import openpyxl

# XLSX source for HCPCT medical procedure codes is taken from:
# https://www.cms.gov/Medicare/Coding/HCPCSReleaseCodeSets/Alpha-Numeric-HCPCS-Items/2018-Alpha-Numeric-HCPCS-File-.html

fp = open('%sHCPC2018_CONTR_ANWEB_disc.xlsx' % (scriptPath))
wb = openpyxl.load_workbook(fp)
ws = wb.active

rowIndex = 0
values = []
descriptors = dict()
for row in ws.rows:
    if rowIndex == 10:
        propertyNames = row
    elif rowIndex > 10:
        columnIndex = 0
        item = dict()
        cellCount = 0
        for cell in row:
            columnName = propertyNames[columnIndex].value
            if cell.value:
                item[columnName] = cell.value
                cellCount += 1
            if rowIndex == 11:
#                print 'COLUMN "%s"' % columnName
                descriptors[columnName] = { 'name': columnName }
                if type(cell) == int:
                    descriptors[columnName] = { 'java': 'java.lang.Integer' }
                elif type(cell) == complex:
                    descriptors[columnName] = { 'java': 'java.lang.Complex' }
                elif type(cell) == float:
                    descriptors[columnName] = { 'java': 'java.lang.Float' }
                elif isinstance(cell,packages.datetime.datetime):
                    descriptors[columnName] = { 'semantic': 'time.moment' }
                elif isinstance(cell,packages.datetime.date):
                    descriptors[columnName]['semantic'] = { 'time.moment' }
                elif isinstance(cell,packages.datetime.time):
                    descriptors[columnName]['semantic'] = { 'time.moment' }
                else:
                    descriptors[columnName] = { 'java': 'java.lang.String' }
            columnIndex += 1
        if cellCount > 0:
            values.append(item)
    rowIndex += 1

print 'DEBUG: There are %d values for %s' % (len(values),scriptPackage)
#print 'DEBUG: descriptors = %s' % packages.json.dumps(descriptors)
#print 'DEBUG: values[0] = %s' % packages.json.dumps(values[0])
#print 'DEBUG: values[#] = %s' % packages.json.dumps(values[len(values)-1])

class MedicalProcedures(packages.patterns.Trait):
    global values,descriptors

    def __init__(self):
        packages.patterns.Trait.__init__(self,scriptPackage,values,descriptors)

exports = [MedicalProcedures]
