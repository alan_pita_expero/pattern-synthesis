class Path:
    def __init__(self,path=[]):
        if len(path) > 0 and type(path[0]) == list:
            raise RuntimeError('Nested path')
        if len(path) > 0 and path[len(path)-1] == '':
            raise RuntimeError('empty path element')
        self.path = path
        self.type = None

    def value(self):
        return self.path

    def type(self):
        return self.type

    def get(self,sub):
        if sub == None:
            return None
        elif type(sub) in(list,tuple):
            return Path(self.path+[str(item) for item in sub])
        elif type(sub) in(str,unicode):
            append = sub.split('.') if sub != '' else []
            return Path(self.path+append)
        else:
            return Path(self.path+[str(sub)])

    def __str__(self):
        return '.'.join([str(item) for item in self.path])

class Variable:
    def __init__(self,path,default):
        self.path = path
        self.allowZero = False
        self.allowMany = False
        self.values = default
        self.changeIndex = None

    def cardinality(self,allowZero,allowMany):
        self.allowZero = allowZero
        self.allowMany = allowMany

    def toDict(self,struct,asSingle=False):
        self.flatten_r(struct,self.path.value(),0,asSingle)

    def fromDict(self,struct):
        self.unflatten_r(struct,self.path.value())

    def flatten_r(self,struct,path,pos,asSingle):
        #print 'DEBUG: BEGIN flatten_r path=%s struct=%s' % ('.'.join(path[0:pos]),struct)
        key = str(path[pos])
        if pos >= len(path):
            raise RuntimeError('zero length property paths are not permitted')
        elif pos == len(path) - 1:
            if type(struct) != dict:
                raise RuntimeError('Expected struct at path %s but found %s' % ('.'.join(path[0:pos]),struct))
            if asSingle:
                if not self.values.isSingle():
                    raise RuntimeError('Single called during flatten_r at %s' % '.'.join(path))
                struct[key] = self.values.single()
            else:
                struct[key] = self.values.multiple()
        else:
            make = key not in struct or struct[key] == None
            if make:
                result = dict()
            else:
                result = struct[key]
            self.flatten_r(result,path,pos+1,asSingle)
            if make:
                struct[key] = result
        #print 'DEBUG: END   flatten_r path=%s struct=%s' % ('.'.join(path[0:pos]),struct)

    def unflatten_r(self,struct,path):
        if len(path) == 0:
            raise RuntimeError('zero length property paths are not permitted')
        elif len(path) == 1:
            key = str(path[0])
            self.values = packages.values.Values([struct[key]])
        else:
            key = str(path[0])
            self.flatten_r(struct.get(key),path[1:])

    def getChangeIndex(self):
        return self.changeIndex

    def getValues(self):
        return self.values

    def setValues(self,values,changeIndex):
        self.values = values
