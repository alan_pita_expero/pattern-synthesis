import values

REDUCE = 'reduce'
CONTINUE = 'continue'
AGAIN = 'again'

class SolvingFailure(Exception):
    def __init__(self,function,path,constraint,values=None):
        self.function = function
        self.path = path
        self.constraint = constraint
        self.values = values

    def __str__(self):
        values = self.values or dict()
        values = ', '.join([ '%s: %s' % (var,val) for var,val in values.iteritems()])
        return 'FAILED => %s %s{%s} (%s)' % (str(self.path),str(self.function),self.constraint,values)

class Constraint:
    def __init__(self,function,binding,priority=True):
        self.function = function
        self.binding = binding
        self.priority = priority

    def __str__(self):
        priority = str(self.priority)
        binding = ['%s: %s' % (key,str(value)) for (key,value) in self.binding.iteritems()]

        return '%s %s%s' % (priority,str(self.function),binding)

    def isMandatory(self):
        return self.priority is True

    def isSelect(self):
        return self.priority is False

    def isBias(self):
        return not (self.priority is True or self.priority is False) and int(self.priority) != 0

    def isArgumentPath(self,changedPropertyPath):
        args = [ arg for arg in self.binding.keys() if str(self.binding.get(arg)) == str(changedPropertyPath)]
        return len(args) == 1

    def project(self,changedPropertyPaths,target):
        changedPropertyPathStrings = [ str(path) for path in changedPropertyPaths ]
        target.trace('BEGIN PROJECT %s' % str(self))
        for arg in self.binding.keys():
            target.trace('     %s' % (self.printArgument(arg,target)))
        args = [ arg for arg in self.binding.keys() if str(self.binding.get(arg)) in changedPropertyPathStrings]
        if len(args) == 0:
            raise RuntimeError('Path %s is not an argument to %s (%d)' % (changedPropertyPath,self,len(args)))
        if self.isMandatory() or self.isSelect():
            result = self.function.project(self,args,target)
        else:
            try:
                result = self.function.project(self,args,target)
            except Exception as e:
                target.trace('Non-mandatory projection FAILURE: %s' % str(e))
                result = CONTINUE
        target.trace('END   PROJECT %s' % str(self))
        return result

    def isArgumentConst(self,argName):
        bound = self.binding.get(argName)
        result = None
        if bound.__class__.__name__ == 'Path':
            return False
        elif bound.__class__.__name__ == 'Values':
            return True

    def printArgument(self,argName,target):
        bound = self.binding.get(argName)
        result = None
        if bound.__class__.__name__ == 'Path':
            result = '%s : %s = %s' % (argName,bound,target.getProperty(bound))
        elif bound.__class__.__name__ == 'Values':
            result = '%s = %s' % (argName,bound)
        if result != None:
            return result
        raise RuntimeError('Cannot print argument %s in %s' % (argName,self.binding))

    def getVariable(self,argName,target):
        bound = self.binding.get(argName)
        result = None
        if bound.__class__.__name__ == 'Path':
            result = bound
        elif bound.__class__.__name__ == 'Values':
            result = None
        return result

    def getArgumentNames(self):
        return self.binding.keys()

    def getArgument(self,argName,target):
        bound = self.binding.get(argName)
        result = None
        if bound.__class__.__name__ == 'Path':
            result = target.getProperty(bound)
        elif bound.__class__.__name__ == 'Values':
            result = bound
        if result != None:
            return result
        raise RuntimeError('Cannot get argument %s in %s' % (argName,self.binding))

    def setArgument(self,argName,target,newValues):
        bound = self.binding.get(argName)
        if bound.__class__.__name__ == 'Path':
            return target.setProperty(self,bound,newValues)
        target.trace('WARNING: Cannot set argument %s in %s' % (argName,self.binding))

class Equals:
    global REDUCE,CONTINUE,AGAIN
    def __str__(self):
      return 'equals'

    def project(self,constraint,args,target):
        reduceCount = 0
        for arg in args:
            a = constraint.getArgument('a',target)
            b = constraint.getArgument('b',target)
            result = None
            if arg == 'b':
              changedVar = 'b'
              otherVar = 'a'
            else:
              changedVar = 'a'
              otherVar = 'b'
            if a.equals(b):
              return CONTINUE
            elif changedVar == 'a' and a.isUniversal():
              result = b.clone()
            elif changedVar == 'b' and b.isUniversal():
              result = a.clone()
            else:
              result = a.intersect(b)
            if result == None:
              raise SolvingFailure(self,path,arg)

            constraint.setArgument(otherVar,target,result)
            reduceCount += 1
        if reduceCount:
            return REDUCE
        else:
            return CONTINUE

class Choice:
    global REDUCE,CONTINUE,AGAIN

    def __str__(self):
      return 'choice'

    def project(self,constraint,args,target):
      a = constraint.getArgument('var',target)
      values = constraint.getArgument('values',target)
      result = None
      if a == values:
          return CONTINUE
      elif type(a) in (tuple,list):
          result = Values([])
          for item in values:
              if item in a and item not in result:
                  result.addValue(item)
          if len(result) == 0:
              result = None
      elif a.isUniversal() or a.overlaps(values):
          result = a.intersect(values)
          if result.equals(a):
              return CONTINUE
      if result == None:
          err = SolvingFailure(self,args,constraint,dict(var=a,values=values))
          raise err
      if constraint.isMandatory() or not result.isEmpty():
          constraint.setArgument('var',target,result)
          return REDUCE
      return CONTINUE

class Lookup:
    global REDUCE,CONTINUE,AGAIN

    def __str__(self):
      return 'lookup'

    def project(self,constraint,args,target):
      container = constraint.getArgument('container',target)
      key       = constraint.getArgument('key',target)
      result    = constraint.getArgument('result',target)

      reduceContainer = False
      reduceKey       = False
      reduceResult    = False
      if 'container' in args:
          reduceKey = not constraint.isArgumentConst('key')
          reduceResult = True
      if 'key' in args:
          # reduce container, result
          reduceContainer = True
          reduceResult = True
      if 'result' in args:
          # reduce container, key
          reduceContainer = True
          reduceKey = not constraint.isArgumentConst('key')

      if container.isUniversal() or container.count() > target.reductionThreshold:
          reduceResult = False
      if result.isUniversal() or result.count() > target.reductionThreshold:
          reduceContainer = False

      if reduceContainer and not key.isUniversal() and not result.isUniversal() and not container.isUniversal():
          newValues = []
          for index in key.multiple():
              target.trace('@value.%s = %s' % (index,result))
              for entry in container.multiple():
                  if entry in newValues:
                      pass
                  elif entry.get(index) not in result.multiple():
                      pass
                  else:
                      newValues.append(entry)
          if newValues != None:
              constraint.setArgument('container',target,newValues)
      if reduceResult and not key.isUniversal() and not container.isUniversal():
          target.trace('@entry = %s' % container)
          newValues = []
          for index in key.multiple():
              for entry in container.multiple():
                  if entry.get(index) not in newValues:
                      newValues += [entry.get(index)]
          if newValues != None:
              constraint.setArgument('result',target,newValues)
      if reduceKey and not container.isUniversal():
          newValues = []
          for index in key.multiple():
              for entry in container.multiple():
                  if entry.get(index) in [ val.get(index) for val in container.multiple() ] and index not in newValues:
                      newValues.append(index)
          if newValues != None:
              constraint.setArgument('key',target,newValues)
      if reduceContainer or reduceKey or reduceResult:
          return REDUCE
      else:
          return CONTINUE
