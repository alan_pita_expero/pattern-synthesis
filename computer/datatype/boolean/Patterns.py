class Boolean(packages.patterns.Enumeration):
    def __init__(self):
        packages.patterns.Enumeration.__init__(self,scriptPackage,[None,True,False])

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Enumeration.injectInto(self,binding)
        target.setProperty(properties,random.uniform() < 0.5)

exports = [Boolean]
