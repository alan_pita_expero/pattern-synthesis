import random

class ComputerIdentity(packages.patterns.Object):
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'name': { 'name': 'name', 'semantic': 'person.name' },
            'username': { 'name': 'username', 'semantic': 'lorem.phrase' },
            'password': { 'name': 'password', 'semantic': 'lorem.phrase' }
        })

    def canEnumerate(self,count):
        return False

    def doEnumerate(self,count):
        raise RuntimeError('Unsupported Method')


exports = [ComputerIdentity]
