import random

class UsernameFormatter:
    def append(self,mode,word,sep):
        value = None
        if not mode.isSingle():
            pass
        elif mode.single() == 'drop':
            value = ''
        elif not word.isSingle():
            pass
        elif mode.single() == 'lower':
            value = word.single().lower()
        elif mode.single() == 'upper':
            value = word.single().upper()
        elif mode.single() == 'caps':
            value = word.single()[0].upper()
            value += word.single()[1:].lower()
        if value == None:
            return None
        if mode.single() == 'drop':
            pass
        elif sep == None:
            pass
        elif not sep.isSingle():
            value = None
        else:
            value += sep.single()
        return value

    def project(self,constraint,args,target):
        mode1  = constraint.getArgument('mode1',target)
        word1  = constraint.getArgument('word1',target)
        sep12  = constraint.getArgument('sep12',target)
        mode2  = constraint.getArgument('mode2',target)
        word2  = constraint.getArgument('word2',target)
        sep23  = constraint.getArgument('sep23',target)
        mode3  = constraint.getArgument('mode3',target)
        word3  = constraint.getArgument('word3',target)

        if args == ['value']:
            target.trace('SKIP - changed arg is %s' % arg)
        else:
            value = ''
            part = self.append(mode1,word1,sep12)
            if part == None:
                target.trace('SKIP - cannot append word1 %s' % args)
                return False
            value += part
            part = self.append(mode2,word2,sep23)
            if part == None:
                target.trace('SKIP - cannot append word2 %s' % args)
                return False
            value += part
            part = self.append(mode3,word3,None)
            if part == None:
                target.trace('SKIP - cannot append word3 %s' % args)
                return False
            value += part
            constraint.setArgument('value',target,packages.values.Values([value]))
            return packages.constraint.REDUCE
        return packages.constraint.CONTINUE


class Username(packages.patterns.Object):
    global UsernameFormatter

    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'mode1': { 'values': ['drop','lower','upper','caps']},
            'word1': { 'name': 'word1', 'semantic': 'lorem.word' },
            'sep12': { 'values': ['.','_','-',''] },
            'mode2': { 'values': ['drop','lower','upper','caps']},
            'word2': { 'name': 'word2', 'semantic': 'lorem.word' },
            'sep23': { 'values': ['.','_','-',''] },
            'mode3': { 'values': ['drop','lower','upper','caps']},
            'word3': { 'name': 'word3', 'semantic': 'lorem.word' },
            'value': { 'encoding': { 'java': 'java.lang.String' } }
        })

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.constrain(UsernameFormatter(),dict(
            mode1=binding.rootPath.get('mode1'),
            word1=binding.rootPath.get('word1'),
            sep12=binding.rootPath.get('sep12'),
            mode2=binding.rootPath.get('mode2'),
            word2=binding.rootPath.get('word2'),
            sep23=binding.rootPath.get('sep23'),
            mode3=binding.rootPath.get('mode3'),
            word3=binding.rootPath.get('word3'),
            value=binding.rootPath.get('value')
        ))

exports = [Username]
