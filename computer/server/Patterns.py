import random
import patterns

# see http://www.vsp.state.va.us/afis/livescan/files/VAEye&HairColor&RaceCodes.pdf

topPublicEmailServers = [
'gmail.com',
'outlook.com',
'yahoo.com',
'aol.com',
'zoho.com',
'mail.com',
'protonmail.com',
'icloud.com'
]

class TopPublicEmailServers(packages.patterns.Enumeration):
    global topPublicEmailServers

    def __init__(self):
        packages.patterns.Enumeration.__init__(self,scriptPackage,topPublicEmailServers)

exports = [TopPublicEmailServers]
