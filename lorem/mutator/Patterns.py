#!/usr/bin/env python

randexp = packages.xeger.Xeger()

charClasses = [
  """[a-z]""",
  """[A-Z]""",
  """\w""",
  """\d""",
  """\W""",
  """\D""",
  """\s""",
  """\S""",
  """[!@#$^&]""",
  """[;:,.?]""",
  """[_|]""",
  """[+\-*/%<>]""",
  """[(){}[]]""",
  """['"]"""
]

# Simple library of words to randomly insert as permutations for strings
wordLibrary = [
  '\n',
  '\t',
  ' ',
  ';',
  ':',
  '.',
  ',',
  '(',
  ')',
  '[',
  ']',
  '{',
  '}',
  'the',
  'be',
  'to',
  'of',
  'and',
  'a',
  'in',
  'that',
  'have',
  'I',
  'it',
  'for',
  'not',
  'on',
  'with',
  'he',
  'as',
  'you',
  'do',
  'at',
  'this',
  'but',
  'his',
  'by',
  'from',
  'they',
  'we',
  'say',
  'her',
  'she',
  'or',
  'an',
  'will',
  'my',
  'one',
  'all',
  'would',
  'there',
  'their',
  'what',
  'so',
  'up',
  'out',
  'if',
  'about',
  'who',
  'get',
  'which',
  'go',
  'me',
  'when',
  'make',
  'can',
  'like',
  'time',
  'no',
  'just',
  'him',
  'know',
  'take',
  'people',
  'into'
]

def charToClassIndex(data):
    """Lookup the class index in charClasses for the character in data"""
    global charClasses
    classIndex = None
    for i in xrange(0,len(charClasses)):
        if classIndex == None and packages.re.match(charClasses[i],data):
            classIndex = i;
    return classIndex

def stringToClasses(data):
    """Map the string onto an array of objects containing each character and its character class index"""
    global charToClassIndex
    result = []
    for i in xrange(0,len(data)):
        result.push({ 'charItem' : data[index], 'charClass': charToClassIndex(data[index]) })
    return result

def classesToWords(data):
    """Map the string onto an array of objects representing consecutive characters of the same character class"""
    accumulator = []
    for item in data:
        if(len(accumulator) == 0):
            accumulator = [item]
        elif(accumulator[len(accumulator) - 1].get('charClass') == item.get('charClass')):
            prev = accumulator.pop()
            prev['charItem'] += item.get('charItem')
            accumulator.push(prev)
        else:
            accumulator.push(item)
    return accumulator

def rand_skip(base,max):
    return (base + packages.random.choice(xrange(0,max))) % max

class MutateCharClass:
    global rand_skip
    global randexp

    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        return type(data) == str and len(data) > 0

    def doMutate(self,data):
        charIndex = packages.random.choice(xrange(0,len(data)))
        classIndex = charToClassIndex(data[charIndex:charIndex+1])
        if classIndex != None:
            newClassIndex = rand_skip(classIndex,len(charClasses))
            prefix = data[0:charIndex]
            suffix = data[charIndex + 1:]
            replacement = randexp.xeger(charClasses[newClassIndex])
        return { 'test': 'Mutate.charClass', 'before': data, 'after': prefix + replacement + suffix }

class MutateCharSwap:
    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        return type(data) == str and len(data) > 1

    def doMutate(self,data):
        index1 = rand_skip(0,len(data))
        index2 = rand_skip(index1,len(data))
        char1 = data[index1]
        char2 = data[index2]

        if(index1 > index2):
            [index1, index2] = [index2, index1]

        a = data[0:index1]
        b = data[index1 + 1:index2]
        c = data[index2 + 1:]

        return { 'test': 'Mutate.charSwap', 'before': data, 'after': a + char2 + b + char1 + c }

class MutateCharDrop:
    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        return type(data) == str and len(data) > 0

    def doMutate(self,data):
        index1 = rand_skip(0,len(data))

        a = data[0:index1]
        b = data[index1 + 1:]

        return { 'test': 'Mutate.charDrop', 'before': data, 'after': a + b }

class MutateCharAdd:
    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        return type(data) == str

    def doMutate(self,data):
        index = 0 if len(data) == 0 else rand_skip(0,len(data)+1)
        a = '' if index == 0  else data[0:index]
        b = '' if index >= len(data) - 1  else data[index:]
        newClassIndex = rand_skip(0,len(charClasses))
        replacement = randexp.xeger(charClasses[newClassIndex])
        return { 'test': 'Mutate.charAdd', 'before': data, 'after': a + replacement + b }

class MutateCharStutter:
    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        type(data) == str and len(data) > 0

    def doMutate(self,data):
        index = rand_skip(0,len(data))

        a =  '' if index == 0 else data[0:index]
        b = data[index]
        c = '' if index >= len(data) - 1 else data[index + 1:]
        return { 'test': 'Mutate.charStutter', 'before': data, 'after': a + b + b + c }

class MutateWordClass:
    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        return type(data) == str and len(data) > 0

    def doMutate(self,data):
        words = classesToWords(stringToClasses(data))
        wordIndex = rand_skip(0,len(words))
        classIndex = words[wordIndex].get('charClass')
        if classIndex != None:
            newClassIndex = rand_skip(classIndex,len(charClasses))
            replacement = randexp.xeger(charClasses[newClassIndex])
            result = ''
            for index in xrange(0,len(words)):
                if index == wordIndex:
                    result += replacement
                else:
                    result += words[index].get('charItem')
        return { 'test': 'Mutate.wordClass', 'before': data, 'after': result }

class MutateWordSwap:
    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        words = classesToWords(stringToClasses(data))
        if type(data) != str or len(data) == 0:
            return False
        return len(words) > 1

    def doMutate(self,data):
        words = classesToWords(stringToClasses(data))
        index1 = rand_skip(0,len(words))
        index2 = rand_skip(index1,len(words))

        result = ''
        for index in xrange(0,len(words)):
            if index == index1:
                result += words[index2].get('charItem')
            elif index == index2:
                result += words[index1].get('charItem')
            else:
                result += words[index].get('charItem')
        return { 'test': 'Mutate.wordSwap', 'before': data, 'after': result }

class MutateWordDrop:
    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        words = classesToWords(stringToClasses(data))
        if type(data) != str or len(data) == 0:
            return False
        return len(words) > 0

    def doMutate(self,data):
        words = classesToWords(stringToClasses(data))
        index1 = random_skip(0,len(words))
        result = ''
        for index in xrange(0,len(words)):
            if index == index1:
                # drop it
                pass
            else:
                result += words[index].get('charItem')
        return { 'test': 'Mutate.wordDrop', 'before': data, 'after': result }

class MutateWordAdd:
    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        words = classesToWords(stringToClasses(data))
        if type(data) != str or len(data) == 0:
            return False
        return len(words) > 0

    def doMutate(self,data):
        words = classesToWords(stringToClasses(data))
        index1 = random_skip(0,len(words)+1)
        result = ''
        for index in xrange(0,len(words)+1):
            if index == index1:
                result += wordLibrary[random_skip(0,len(wordLibrary))]
            if index < len(words):
                result += words[index].get('charItem')
        return { 'test': 'Mutate.wordAdd', 'before': data, 'after': result }

class MutateWordStutter:
    def __init__(self):
        self.count = 1

    def canMutate(self,data):
        words = classesToWords(stringToClasses(data))
        if type(data) != str or len(data) == 0:
            return False
        return len(words) > 0

    def doMutate(self,data):
        words = classesToWords(stringToClasses(data))
        index1 = random_skip(0,len(words))
        result = ''
        for index in xrange(0,len(words)):
            if index == index1:
                result += words[index].get('charItem')
            result += words[index].get('charItem')
        return { 'test': 'Mutate.wordStutter', 'before': data, 'after': result }

class MutationFormatter:
    global MutateCharClass,MutateCharSwap,MutateCharAdd,MutateCharDrop,MutateCharStutter

    def __str__(self):
      return 'mutation_formatter'

    def project(self,constraint,args,target):
        plain    = constraint.getArgument('plain',target)
        mutation = constraint.getArgument('mutation',target)

        if 'plain' in args and plain.isSingle() and mutation.isUniversal():
            klass = packages.random.choice([MutateCharClass,MutateCharSwap,MutateCharAdd,MutateCharDrop,MutateCharStutter])
            mutator = klass()
            constraint.setArgument('mutation',target,[mutator.doMutate(plain.single())])
            return packages.constraint.REDUCE
        packages.constraint.CONTINUE

class Mutator(packages.patterns.Object):
    global MutationFormatter

    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'plain': { 'encoding': { 'java': 'java.lang.String' }},
            'mutation': { 'encoding': { 'java': 'java.lang.String' }},
        })

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.constrain(MutationFormatter(),dict(plain=properties.get('plain'),mutation=properties.get('mutation')))

#for klass in [MutateCharClass,MutateCharSwap,MutateCharAdd,MutateCharDrop,MutateCharStutter]:
#    mutate = klass()
#    data = 'Fourscore and Seven Years ago'
#    print 'CLASS:  %s' % klass.__name__
#    print 'BEFORE: %s' % data
#    print 'AFTER : %s' % mutate.doMutate(data)

exports = [Mutator]
