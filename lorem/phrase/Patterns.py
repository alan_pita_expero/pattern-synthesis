fp = open('%s../english-words.txt' % (scriptPath))
db = packages.random.sample([ word.strip() for word in fp ],100)

class Words(packages.patterns.BasePattern):
    global db
    
    def __init__(self):
        packages.patterns.BasePattern.__init__(self,scriptPackage)

    def canEnumerate(self,count):
        return False

    def doEnumerate(self,count):
        raise RuntimeError('Unsupported Method')

    def word(self):
        return packages.random.choice(db)

    def words(self):
        length = packages.random.choice(xrange(1,7))
        return ' '.join([ self.word() for i in xrange(0,length) ])

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        target.declareProperty(binding)
        packages.patterns.BasePattern.injectInto(self,binding)
        target.setProperty(self,properties.value(),self.words())

exports = [Words]
