#!/usr/bin/python

import json
import sys

def idToString(id):
    """Take an entity ID object in genericJSON format and reformat it as a simple string for use as aux key"""
    return '%d' % int('0x%s' % id.get('partition_key'),0)

def getPersonByVertexId(data):
    """Traverse vertices + edges in genericJSON data, build table from vertex id to Person object in genericJSON"""
    personById = dict()
    for (key,item) in data.get('person').iteritems():
        personId = idToString(item.get('id'))
        personById[personId] = item
    if len(personById.keys()) == 0:
        raise RuntimeError('Empty table produced by getPersonByVertexId')
    return personById

def getEventByVertexId(data):
    """Traverse vertices + edges in genericJSON data, build table from vertex id to Event object in genericJSON"""
    eventById = dict()
    for (key,item) in data.get('datacube').get('journey').get('stage').iteritems():
        eventId = idToString(item.get('id'))
        eventById[eventId] = item
    if len(eventById.keys()) == 0:
        raise RuntimeError('Empty table produced by getEventByVertexId')
    return eventById

def participatesIn(data,expectedFrom,expectedTo):
    if type(expectedFrom) not in (str,unicode):
        expectedFrom = idToString(expectedFrom)
    if type(expectedTo) not in (str,unicode):
        expectedTo   = idToString(expectedTo)
    for (key,item) in data.get('datacube').get('participatesIn').iteritems():
        if idToString(item.get('from')) == expectedFrom and idToString(item.get('to')) == expectedTo:
            return True
    return False

data = json.load(sys.stdin)
datacube = data.get('datacube')

if 'journey' not in datacube:
    raise RuntimeError('Cannot locate journey within data')
if 'stage' not in datacube.get('journey'):
    raise RuntimeError('Cannot locate journey within data.journey')

allStages = datacube.get('journey').get('stage')

personByVertexId = getPersonByVertexId(data)
eventByVertexId = getEventByVertexId(data)

errCount = 0

for personId in personByVertexId.keys():
    personVertex = personByVertexId.get(personId)
    familyName = personVertex.get('properties').get('name').get('family').get('phrase')
    givenName = personVertex.get('properties').get('name').get('given').get('phrase')
    stages = dict()
    for index in sorted(allStages.keys(),key=int):
        stage = allStages.get(str(index))
        if participatesIn(data,personId,stage.get('id')):
            stages[str(index)] = stage
    print 'CHECKING TIMESTAMPS for %s, %s' % (familyName,givenName)
    lastDt = None
    for index in sorted(stages.keys(),key=int):
        stage = stages.get(str(index))
        startDt = stage.get('properties').get('period').get('start').get('iso')
        endDt = stage.get('properties').get('period').get('end').get('iso')
        if lastDt and startDt != lastDt:
            errCount += 1
            print 'ERROR: time lost from %s to %s' % (lastDt,startDt)
        lastDt = endDt            
        print 'Stage index %d name %s from %s to %s' % (int(index),stage.get('properties').get('type').get('name'),startDt,endDt)

    print 'CHECKING Locations for %s, %s' % (familyName,givenName)
    lastLocation = None
    for index in sorted(stages.keys(),key=int):
        stage = stages.get(str(index))
        isTransfer = stage.get('properties').get('type').get('dataType') == 'transfer'
        isAbroad = stage.get('properties').get('type').get('dataType') == 'abroad'
        if isTransfer or isAbroad:
            fromLocation = stage.get('properties').get('fromLocation').get('@value').get('code')
            toLocation = stage.get('properties').get('toLocation').get('@value').get('code')
            print 'Stage index %d name %s from %s to %s' % (int(index),stage.get('properties').get('type').get('name'),fromLocation,toLocation)
            if lastLocation and fromLocation != lastLocation:
                errCount += 1
                print 'ERROR: spontaneous movement from %s to %s' % (lastLocation,fromLocation)
            lastLocation = toLocation
        else:
            location = stage.get('properties').get('fromLocation').get('@value').get('code')
            print 'Stage index %d name %s at %s' % (int(index),stage.get('properties').get('type').get('name'),location)
            if lastLocation and location != lastLocation:
                errCount += 1
                print 'ERROR: spontaneous movement from %s to %s' % (lastLocation,location)
            lastLocation = location

sys.exit(0 if errCount == 0 else 1)
