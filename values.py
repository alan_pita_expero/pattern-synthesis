import random
import itertools

class Values:
    def __init__(self,values=None):
        if isinstance(values,Values):
            raise RuntimeError('Nested values!')
        elif type(values) in (list,tuple) and len(values) > 0 and isinstance(values[0],Values):
            raise RuntimeError('Nested values!')
        elif hasattr(values,'__iter__'):
            slice2 = list(itertools.islice(values,2))
            if len(slice2) == 1:
                values = [slice2[0]]
        self.values = values

    def __rep__(self):
        if self.values == None:
            return '[*]'
        else:
            values = list(itertools.islice(self.values,16))
            return 'Values|%d|%s' % (len(self.values),str(values))

    def __str__(self):
        if self.values == None:
            return '[*]'
        else:
            values = list(itertools.islice(self.values,16))
            return 'Values|%d|%s' % (len(self.values),str(values))

    def count(self):
        return len(self.values)

    def single(self):
        if self.values == None:
            raise RuntimeError('single called on universal value set')
        elif len(self.values) != 1:
            raise RuntimeError('single called on non unique value set %s' % self)
        return self.values[0]

    def multiple(self):
        return self.values

    def isUniversal(self):
        return self.values == None

    def isEmpty(self):
        if self.values == None:
            return False
        slice1 = list(itertools.islice(self.values,1))
        return len(slice1) == 0

    def hasFewerValues(self,count):
        if self.values == None:
            return False
        slice2 = list(itertools.islice(self.values,count+1))
        return len(slice2) <= count

    def isSingle(self):
        if self.values == None:
            return False
        if type(self.values) in (list,tuple):
            return len(self.values) == 1
        return False

    def clone(self):
        result = Values()
        if(self.values != None):
            if type(self.values) in (list,tuple):
                result.values = self.values[:]
            else:
                result.values = self.values
        return result

    def overlaps(self,other):
        if not isinstance(other,Values):
            raise RuntimeError('Cannnot compare sets %s and %s' % (self,other))
        if self.values == None or other.values == None:
            return True
        for item in self.values:
            if item in other.values:
                return True
        return False

    def equals(self,other):
        if not isinstance(other,Values):
            raise RuntimeError('Cannnot compare sets %s and %s' % (self,other))
        if self.values == None and other.values == None:
            return True
        elif self.values == None or other.values == None:
            return False

        a_pos = 0
        b_pos = 0
        a_sorted = list(self.values)
        b_sorted = list(other.values)
        a_sorted.sort()
        b_sorted.sort()
        if len(a_sorted) != len(b_sorted):
            return False
        while a_pos < len(a_sorted) and b_pos < len(b_sorted):
            if a_sorted[a_pos] < b_sorted[b_pos]:
                return False
            elif a_sorted[a_pos] > b_sorted[b_pos]:
                return False
            else:
                a_pos += 1
                b_pos += 1

        return True

    def isProperSubsetOf(self,other):
        for item in self.values:
            if item not in other.values:
                return True
        return False

    def intersect(self,other):
        if not isinstance(other,Values):
            other = Values(other)
        if self.values == None:
            return other.clone()
        elif other.values == None:
            return self.clone()
        else:
            result = []
            a_pos = 0
            b_pos = 0
            a_sorted = list(self.values)
            b_sorted = list(other.values)
            a_sorted.sort()
            b_sorted.sort()
            while a_pos < len(a_sorted) and b_pos < len(b_sorted):
                if a_sorted[a_pos] < b_sorted[b_pos]:
                    a_pos += 1
                elif a_sorted[a_pos] > b_sorted[b_pos]:
                    b_pos += 1
                else:
                    result.append(a_sorted[a_pos])
                    a_pos += 1
                    b_pos += 1

            #print 'DEBUG: intersect a = %s' % a_sorted
            #print '                 b = %s' % b_sorted
            #print '                 r = %s' % result
            return Values(result)

    def reduce(self,path,target):
        reductionThreshold = target.reductionThreshold
        if self.values == None:
            target.trace('WARNING: cannot randomize %s' % path)
            return None
        elif hasattr(self.values,'isUniversal') and self.values.isUniversal():
            target.trace('WARNING: cannot randomize %s' % path)
            return None
        elif type(self.values) in (list,tuple):
            if len(self.values) in (0,1):
                pass
            elif type(self.values[0]) == dict and '@weight' in self.values[0]:
                if len(self.values) <= reductionThreshold:
                    pass
                else:
                    pass
            elif len(self.values) <= reductionThreshold:
                selected = random.choice(self.values)
                newValues = self.values[:]
                newValues.remove(selected)
                target.trace('DEBUG: random reduction on variable %s with value %s removes %s' % (path,self.values,selected))
                return Values(newValues)
            else:
                count = reductionThreshold if reductionThreshold > len(self.values)/2 else len(self.values)/2
                newValues = random.sample(self.values,count)
                return Values(newValues)
        elif hasattr(self.values,'__iter__'):
            slice2 = list(itertools.islice(self.values,2))
            if len(slice2) == 1:
                newValues = Values([slice2[0]])
                target.trace('DEBUG: random reduction on variable %s with value %s selects %s' % (path,self.values,newValues))
                return newValues
            try:
                newValues = Values(random.sample(self.values,reductionThreshold))
                target.trace('DEBUG: random reduction on variable %s with value %s selects %s' % (path,self.values,newValues))
                return newValues
            except Exception as e:
                return Values([random.choice(self.values)])
        else:
            target.trace('WARNING: type of values is %s' % type(self.values))
            return None
