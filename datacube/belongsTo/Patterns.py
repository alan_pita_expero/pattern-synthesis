class BelongsTo(packages.patterns.Edge):
    def __init__(self):
        packages.patterns.Edge.__init__(self,scriptPackage,'belongs_to','datacube.vertex.id',{
            'role': { 'values': ['FMUA', 'CRMA', 'FIRA', 'SOTA', 'PERA', 'SGDA'] }
        })

exports = [BelongsTo]
