import random

class t_person_same_as(packages.patterns.BasePattern):
    def __init__(self):
        packages.patterns.BasePattern.__init__(self,scriptPackage)

    def canEnumerate(self,count):
        return False

    def doEnumerate(self,count):
        raise RuntimeError('Unsupported Method')

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.BasePattern.injectInto(self,binding)
        count = packages.random.choice(xrange(2,8))
        target.setProperty(self,properties.get('count'),count)
        t_person_list = []
        for i in xrange(0,count):
            subPath = '%s.t_person.%d' % ('.'.join(properties.value()),i)
            target.makeEntity( subPath,'datacube.vertex.t_person')
            t_person_list.append(subPath)

        memberPath = t_person_list[0].split('.')
        memberIndex = 0
        fromVertex = target.getPattern(properties.get('t_person.%d' % 0))
        for member in t_person_list:
            if memberIndex > 0:
                memberPath = member.split('.')
                toVertex = target.getPattern(properties.get('t_person.%d' % memberIndex))
                edgePath = properties.get('t_same_as.%d' % memberIndex)
                edge = target.makeEntity(edgePath,'datacube.edge.id')
                target.setProperty(self,edgePath.split('.')+['~label'],'t_same_as')
                edge.get("from").sync(fromVertex.get('id'))
                edge.get("to").sync(toVertex.get('id'))
            memberIndex += 1

exports = [t_person_same_as]
