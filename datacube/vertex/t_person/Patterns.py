import random

class t_person(packages.patterns.Object):
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'id': { 'name': 'id', 'semantic': 'datacube.vertex.id' },
            'DOB': { 'name': 'DOB', 'semantic': 'person.event.birth.date' },
            'name': { 'name': 'name', 'semantic': 'person.name' },
            'gender': { 'name': 'gender', 'semantic': 'person.biometric.gender' }
        })

exports = [t_person]
