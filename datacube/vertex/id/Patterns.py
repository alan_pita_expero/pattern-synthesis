class VertexId(packages.patterns.Object):
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            '@class': { 'values': ['vertex'] },
            'label': { 'encoding': { 'java': 'java.lang.String' } },
            'cluster_key': { 'values': ['synthetic'] },
            'partition_key': { 'name': 'partition_key', 'semantic': 'computer.datatype.identifier.formal' }
        })

exports = [VertexId]
