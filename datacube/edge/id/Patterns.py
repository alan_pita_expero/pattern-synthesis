import random

class EdgeId(packages.patterns.Object):
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'from': { 'name': 'from', 'semantic': 'datacube.vertex.id' },
            'to': { 'name': 'to', 'semantic': 'datacube.vertex.id' }
        })

exports = [EdgeId]
