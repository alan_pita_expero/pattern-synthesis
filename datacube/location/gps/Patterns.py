# -*- coding: utf-8 -*-
import random
import patterns

# See source: https://www.maps.ie/coordinates.html
# NOT LICENSED FOR PRODUCTION USE
locations = [
  #{ "lng": -115.81352721704104, "lat": 37.24812085511815,  "name": u"Area 51 - Groom Lake, NV", "code": "AREA51" },
  #{ "lng": 0.0,                 "lat": 0.0,                "name": u"Unknown", "code": "" },
  #{ "lng": 0.0,                 "lat": 0.0,                "name": u"PDN Station", "code": "PDN" },
  { "lng": -97.50810509268302,  "lat": 26.020478361493986, "name": u"BRP Station", "code": "BRP" },
  { "lng": -116.44849440000002, "lat": 32.7194101,         "name": u"U.S. Customs and Border Protection - San Diego Campo Station", "code": "SDC" },
  { "lng": -100.50611409999999, "lat": 28.705596,          "name": u"U.S. Customs and Border Protection - Eagle Pass Station", "code": "EGS" },
  { "lng": -110.9427210870773,  "lat": 31.333013510926893, "name": u"U.S. Customs and Border Protection - Nogales Port of Entry", "code": "NOG" },
  { "lng": -112.73972750000001, "lat": 32.2739626,         "name": u"U.S. Customs and Border Protection - Tucson Ajo Station", "code": "AJO" },
  { "lng": -117.05238989999998, "lat": 32.5650925,         "name": u"U.S. Customs and Border Protection – Chula Vista Station", "code": "CHU" },
  { "lng": -98.13839949999999,  "lat": 27.0250973,         "name": u"U.S. Customs and Border Protection – Falfurrias Station", "code": "FLF" },
  { "lng": -98.80142669999998,  "lat": 26.36729,           "name": u"U.S. Customs and Border Protection – Rio Grande City Station", "code": "RGC" },
  { "lng": -97.4916265,         "lat": 25.8984584,         "name": u"U.S. Customs and Border Protection – Fort Brown Station", "code": "FTB" },
  { "lng": -97.94949489999999,  "lat": 26.06331,           "name": u"U.S. Customs and Border Protection – Weslaco Station", "code": "WSL" },
  { "lng": -97.7372631,         "lat": 26.0382149,         "name": u"U.S. Customs and Border Protection – Harlingen Station", "code": "HRL" },
  { "lng": -115.49115863803712, "lat": 32.665256221458634, "name": u"US Immigration & Customs Enfc - Calexico", "code": "ELS" },
  { "lng": -114.59904230000001, "lat": 32.6683586,         "name": u"U.S. Customs and Border Protection – Yuma Station", "code": "YUS" },
  { "lng": -106.4421979,        "lat": 31.8726665,         "name": u"U.S. Customs and Border Protection – El Paso Station", "code": "EPS" },
  { "lng": -99.50725149265782,  "lat": 27.50191601929117,  "name": u"U.S. Customs and Border Protection – Laredo Convent Avenue Port of Entry", "code": "LRT" },
  { "lng": -110.89370689999998, "lat": 32.193941,          "name": u"U.S. Customs and Border Protection - Tucson Station", "code": "TCA" },
  { "lng": -98.26511970000001,  "lat": 26.1528643,         "name": u"U.S. Customs and Border Protection - McAllen Station", "code": "MCS" },
  { "lng": -97.49643120000002,  "lat": 25.8989305,         "name": u"U.S. Customs and Border Protection - Brownsville Port of Entry", "code": "RGV" },
  #{ "lng": -80.27519159999997,  "lat": 25.7970602,         "name": u"U.S. Customs and Border Protection - Miami International Airport", "code": "MIA" },
  #{ "lng": -84.39764660000003,  "lat": 33.639692,          "name": u"U.S. Customs and Border Protection - Hartsfield-Jackson Atlanta International Airport", "code": "ATL"},
  #{ "lng": -95.35053449999998,  "lat": 29.9380721,         "name": u"U.S. Customs and Border Protection - Houston Intercontinental Airport", "code": "IAH" },
  #{ "lng": -112.015783,         "lat": 33.4246587,         "name": u"U.S. Customs and Border Protection - Phoenix Sky Harbor International Airport", "code": "PHX" },
  #{ "lng": -118.40862679999998, "lat": 33.943672,          "name": u"U.S. Customs and Border Protection - Los Angeles International Airport", "code": "LAXAIR" },
  #{ "lng": -87.87314909999998,  "lat": 41.9797319,         "name": u"U.S. Customs and Border Protection - Chicago O'hare International Airport", "code": "ORD" },
  #{ "lng": -83.3521414,         "lat": 42.2111972,         "name": u"U.S. Customs and Border Protection - Detroit Metropolitan Port of Entry", "code": "DTW" },
  #{ "lng": -73.78285749999998,  "lat": 40.6435272,         "name": u"U.S. Customs and Border Protection - John F. Kennedy International Airport", "code": "JFK" }
]

class Locations(packages.patterns.Trait):
    global locations

    def __init__(self):
        packages.patterns.Trait.__init__(self,scriptPackage,locations,{
            'lng': { 'encoding': { 'java': 'java.lang.Float' } },
            'lat': { 'encoding': { 'java': 'java.lang.Float' } },
            'name': { 'encoding': { 'java': 'java.lang.String' } },
            'code': { 'encoding': { 'java': 'java.lang.String' } }
        })

class Anywhere(packages.patterns.BasePattern):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.BasePattern.injectInto(self,binding)
        if target.hasProperty(properties.get('coordinates')):
            lat = random.choice(-180.0,180.0)
            lng = random.choice(-180.0,180.0)

            latProp = properties.upsert('coordinates','lat')
            lngProp = properties.upsert('coordinates','lng')

            target.setProperty(latProp,lat)
            target.setProperty(lngProp,lng)

exports = [Locations]
