import os
import sys

scriptPath = os.path.dirname(os.path.realpath(__file__))
sys.path.append('%s/..' % scriptPath)
import target

target = target.Target()
target.makeEntity('t_person_same_as','datacube.neighbors.t_person_same_as')
target.close()
print unicode(target.toDict()).encode('utf-8')
