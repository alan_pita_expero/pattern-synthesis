import random

fp = open('%sstageTypes.json' % (scriptPath))
db = packages.json.load(fp)
stageTypes = db.get('stageTypes')
stageTransitions = db.get('stageTransitions')

def nextState(target,state,stage):
    global stageTransitions
    prevState = state
    transCandidates = [ trans for trans in stageTransitions if trans.get('from') == state ]
    if len(transCandidates) == 0:
        raise RuntimeError('No state can follow %s' % state)
    elif len(transCandidates) == 1:
        state = transCandidates[0].get('to')
    else:
        state = packages.random.choice([ trans.get('to') for trans in transCandidates ])

    stateCandidates = [type for type in stageTypes if type.get('dataType') == state ]
    values = packages.values.Values(stateCandidates)
    if values.isEmpty():
        raise RuntimeError('No stageType entries for state %s' % state)
    typeVar = stage.get('properties').get('type')

    target.constrain(packages.constraint.Choice(),dict(var=typeVar,values=values))
    if state not in ('transfer','abroad'):
        stage.get('properties.toLocation').sync(stage.get('properties.fromLocation'))
    return state

class Journey(packages.patterns.Vertex):
    global nextState

    def __init__(self):
        packages.patterns.Vertex.__init__(self,scriptPackage,'Journey','datacube.vertex.id',{
            'stageCount': { 'encoding': { 'java': 'java.lang.Long' } }
        })

    global stageTypes

    def makeGroup(self,binding,index):
        contGroup = packages.patterns.Collection('datacube.group',[1])
        contGroup.addOwner(self)
        contGroup.injectInto(binding.get('datacube.group').get(str(index)))
        groups = contGroup.getInjectedItems()
        return groups[0]

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Vertex.injectInto(self,binding)
        stageCount = packages.random.choice(xrange(3,7))
        target.setProperty(self,properties.get('properties').get('stageCount'),stageCount)

    def injectNeighbors(self,binding):
        target = binding.getTarget()
        contParticipant = packages.patterns.Collection('person',xrange(5,12))
        contParticipant.addOwner(self)
        contParticipant.injectInto(binding.get('participant'))

        stageCount = binding.getTarget().getProperty(binding.getRootPath().get('properties').get('stageCount')).single()
        contStages1 = packages.patterns.Collection('datacube.journey.stage',[stageCount])
        contStages1.addOwner(self,0,1) # should be 1,1
        contStages1.injectInto(binding.get('stages1'))
        contStages1.makeEdgeToId('datacube.partOf',binding.get('id'))
        contStages1.makePropertyIndex('stageIndex')
        stages1 = contStages1.getInjectedItems()

        state = None
        prev = None
        numGroups = 0
        for stage in stages1:
            state = nextState(target,state,stage)
            contParticipant.makeEdgeToId('datacube.participatesIn',stage.get('id'))
            if prev != None:
                prev.get('properties.period.end').sync(stage.get('properties.period.start'))
                prev.get('properties.toLocation').sync(stage.get('properties.fromLocation'))
            prev = stage
        lastState = state
        lastPrev = prev

        prevParticipant = None
        sameFamily = []
        for participant in contParticipant.getInjectedItems():
            code = packages.random.choice(xrange(0,100))
            if prevParticipant != None and participant != prevParticipant and code < 75:
                participant.get('properties.name.family').sync(prevParticipant.get('properties.name.family'))
                sameFamily.append(True)
            else:
                sameFamily.append(False)
                group = self.makeGroup(binding,numGroups)
                numGroups += 1
            participant.makeEdgeToId('datacube.belongsTo',group.get('id'))
            participant.get('properties.name.family').sync(group.get('familyName'))
            prevParticipant = participant

        participantIndex = 0
        remainder = contParticipant.getInjectedItems()
        partitionCount = 0
        while len(remainder) > 0:
            # construct a non-empty partition of people
            partition = []
            done = len(remainder) == 0
            isSameFamily = False
            while not done:
                if len(partition) == 0:
                    # no empty partitions
                    partition.append(remainder[0])
                    remainder = remainder[1:]
                elif isSameFamily and packages.random.choice(xrange(0,100)) > 80:
                    # same family means same partition 80%
                    partition.append(remainder[0])
                    remainder = remainder[1:]
                elif not isSameFamily and packages.random.choice(xrange(0,100)) > 60:
                    # diff family means same partition 60%
                    partition.append(remainder[0])
                    remainder = remainder[1:]
                else:
                    # finish this partition
                    done = True

                if len(remainder) == 0:
                    done = True
                if not done:
                    sameFamily = sameFamily[1:]
                    isSameFamily = sameFamily[0]

            stageCount = packages.random.choice(xrange(1,7))
            contStages2 = packages.patterns.Collection('datacube.journey.stage',[stageCount])
            contStages2.addOwner(self,0,1) # should be 1,1
            contStages2.injectInto(binding.get('stages2.%d' % participantIndex))
            contStages2.makeEdgeToId('datacube.partOf',binding.get('id'))
            contStages2.makePropertyIndex('stageIndex',len(stages1))
            stages2 = contStages2.getInjectedItems()

            prevStage = lastPrev
            state = lastState
            for stage in stages2:
                state = nextState(target,state,stage)
                if prevStage != None:
                    prevStage.get('properties.period.end').sync(stage.get('properties.period.start'))
                    prevStage.get('properties.toLocation').sync(stage.get('properties.fromLocation'))
                prevStage = stage

            for participant in partition:
                for stage in stages2:
                    participant.makeEdgeToId('datacube.participatesIn',stage.get('id'))
                participantIndex += 1
            partitionCount += 1

        expectedParticipants = len(contParticipant.getInjectedItems())
        if participantIndex != expectedParticipants:
            raise RuntimeError('Expected there to be %d participants in %d partitions but there were %d' % (expectedParticipants,partitionCount,participantIndex))
        else:
            target.trace('journey: there were %d participants in %d partitions' % (expectedParticipants,partitionCount))
        return None

exports = [Journey]
