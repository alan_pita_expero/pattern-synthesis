import random

fp = open('%s../stageTypes.json' % (scriptPath))
db = packages.json.load(fp)
stageTypes = db.get('stageTypes')

class Event(packages.patterns.Vertex):
    global stageTypes

    def __init__(self):
        packages.patterns.Vertex.__init__(self,scriptPackage,'Stage','datacube.vertex.id',{
            'fromLocation': { 'name': 'fromLocation', 'semantic': 'datacube.location.gps' },
            'toLocation': { 'name': 'toLocation', 'semantic': 'datacube.location.gps' },
            'source': { 'values': [ 'MCAT' ] },
            'avgTimeToDisposition': { 'values': xrange(0,99) },
            'complianceScore': { 'values': xrange(0,100) },
            'type': { 'values': stageTypes },
            'agency': { 'values': ['USBP', 'OFO', 'OA', 'G4S', 'ERO'] },
            'period': { 'name': 'period', 'semantic': 'time.period' },
            'operationName': { 'name': 'operationName', 'semantic': 'datacube.operationName' },
            'notes': { 'name': 'notes', 'semantic': 'lorem.phrase' }
        })

exports = [Event]
