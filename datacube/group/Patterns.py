import random

class Family(packages.patterns.Object):
    def __init__(self):
        self.owners = []
        packages.patterns.Object.__init__(self,scriptPackage,{
            'id': { 'name': 'id', 'semantic': 'datacube.vertex.id' },
            'type': { 'values': ['family'] },
            'familyName': { 'semantic': 'person.name.family' }
        })

    global roles

    def addOwner(self,owner):
        if type(owner) in (tuple,list):
            self.owners += owner
        else:
            self.owners.append(owner)

    def allocate(self,properties,target):
        target.setProperty(self,properties.get('count'),packages.values.Values())

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.constrain(packages.constraint.Choice(),dict(var=properties.get('id').get('label'),values=packages.values.Values(['Family'])))

class Ramos(packages.patterns.Object):
    def __init__(self):
        self.owners = []
        packages.patterns.Object.__init__(self,scriptPackage,{
            'id': { 'name': 'id', 'semantic': 'datacube.vertex.id' },
            'type': { 'values': ['family'] },
            'familyName': { 'encoding': { 'java': 'java.lang.String' } }
        })

    global roles

    def addOwner(self,owner):
        if type(owner) in (tuple,list):
            self.owners += owner
        else:
            self.owners.append(owner)

    def allocate(self,properties,target):
        target.setProperty(self,properties.get('count'),packages.values.Values())

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.constrain(packages.constraint.Choice(),dict(var=properties.get('id').get('label'),values=packages.values.Values(['Family'])))

        target.constrain(packages.constraint.Choice(),dict(var=properties.get('familyName'),values=packages.values.Values(['Ramos'])))

    def injectNeighbors(self,binding):
        contPerson = packages.patterns.Collection('person',[3])
        contPerson.addOwner(self.owners,0,1)
        contPerson.injectInto(binding.get('people'))
        contPerson.makeEdgeToId('datacube.belongsTo',binding.get('id'))
        contPerson.syncEachProperty('properties.name.family.phrase',binding.get('familyName'))
        persons = contPerson.getInjectedItems()

        persons[0].applyTemplate({
            'properties': {
                'name': {
                    'given': { 'phrase': 'Tina' },
                },
                'alienNum': 859361509,
                'citizenship': {
                    'name': 'Mexico'
                },
                'gender': 'Female',
                'dob': {
                    'birthDate': {
                        'iso': '2000-08-09T00:00:00-06:00'
                    }
                }
            }
        })

        persons[1].applyTemplate({
            'properties': {
                'name': {
                    'given': { 'phrase': 'Gina' }
                },
                'alienNum': 859361510,
                'citizenship': {
                    'name': 'Mexico'
                },
                'gender': 'Female',
                'dob': {
                    'birthDate': {
                        'iso': '1983-02-14T00:00:00-06:00'
                    }
                }
            }
        })

        persons[2].applyTemplate({
            'properties': {
                'name': {
                    'given': { 'phrase': 'Pablo' }
                },
                'alienNum': 859361511,
                'citizenship': {
                    'name': 'Mexico'
                },
                'gender': 'Female',
                'dob': {
                    'birthDate': {
                        'iso': '1966-06-10T00:00:00-06:00'
                    }
                }
            }
        })

exports = [Family]
#exports = [Ramos]
