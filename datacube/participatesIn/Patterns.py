class ParticipatesIn(packages.patterns.Edge):
    def __init__(self):
        packages.patterns.Edge.__init__(self,scriptPackage,'participates_in','datacube.vertex.id',{
            'age': { 'values': xrange(0,140) },
            'accompanied': { 'values': [ 'Y','N','UNK' ] },
            'relationship': { 'values': [ 'FMU','FMG','null'] }
        })

exports = [ParticipatesIn]
