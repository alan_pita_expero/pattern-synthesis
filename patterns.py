import random
import constraint
import values
import exrex

class Binding:
    def __init__(self,target,rootPath=None):
        self.pattern = None
        self.target = target
        if target.__class__.__name__ != 'Target':
            raise RuntimeError('Wrong class %s for argument target' % target.__class__.__name__)
        if type(rootPath) == str:
            rootPath = self.target.lookup(rootPath)
        if rootPath.__class__.__name__ != 'Path':
            raise RuntimeError('Expecting an object of type Path for rootPath but got %s' % rootPath.__class__.__name__)
        self.rootPath = rootPath
        self.sub = dict()

    def __str__(self):
        result = ''
        if self.pattern:
            result += 'PATTERN %s ' % self.pattern
        result += 'AT %s' % self.rootPath
        return result

    def getRootPath(self):
        return self.rootPath

    def getTarget(self):
        return self.target

    def get(self,name):
        if name == '':
            return self
        pathList = name.split('.')
        result = self
        #print 'DEBUG: pathList = %s' % pathList
        for pathItem in pathList:
            #print 'BEFORE %s result = %s' % (pathItem,result)
            if pathItem not in result.sub:
                result.sub[pathItem] = Binding(self.target,result.nextPath(pathItem))
            result = result.sub.get(pathItem)
        #print 'DEBUG: pathList = %s result = %s' % (pathList,result)
        return result

    def nextPath(self,pathItem):
        if self.pattern:
            return self.pattern.nextPath(self.rootPath,pathItem)
        else:
            return self.rootPath.get(pathItem)

    def set(self,pattern):
        if self.pattern != None and self.pattern != pattern:
            raise RuntimeError('Multiple values for Binding.pattern at %s\n    %s\n    %s' % (self.rootPath,self.pattern,pattern))
        self.pattern = pattern

    def applyTemplate(self,template):
        if type(template) == dict:
            for key,value in template.iteritems():
                self.get(key).applyTemplate(value)
        else:
            self.target.constrain(constraint.Choice(),dict(var=self,values=values.Values([template])))

    def collectVariablePaths(self,variablePaths):
        return self.collectVariablePaths_r(self.pattern,self.rootPath,[],variablePaths)

    def collectVariablePaths_r(self,pattern,rootPath,relPathList,variablePaths):
        if hasattr(pattern,'properties'):
            for propDefn in pattern.properties:
                propName = propDefn.name
                if propName:
                    sub = self.get(propName)
                    sub.collectVariablePaths_r(sub.pattern,rootPath,relPathList+[propName],variablePaths)
                else:
                    relPath = '.'.join(relPathList)
                    if self.target.hasProperty(self.rootPath) and relPath not in variablePaths:
                        variablePaths.append(relPath)
        elif hasattr(pattern,'collectVariablePaths'):
            pattern.collectVariablePaths(self,relPathList,variablePaths)
        else:
            relPath = '.'.join(relPathList)
            if self.target.hasProperty(self.rootPath) and relPath not in variablePaths:
                variablePaths.append(relPath)

    def sync(self,other,exceptProps=[]):
        variablePaths = []
        self.collectVariablePaths(variablePaths)
        for path in variablePaths:
            disqualify = [ exceptProp for exceptProp in exceptProps if path == exceptProp or path.startswith(exceptProp) ]
            if len(disqualify) == 0:
                self.getTarget().constrain(constraint.Equals(),dict(a=self.get(path),b=other.get(path)))

    def makeEdgeToId(self,edgeLabel,toId):
       edges = self.target.injectEntities(edgeLabel,1,0,1)
       edges[0].get('from').sync(self.get('id'))
       edges[0].get('to').sync(toId)

    def makeEdgeFromId(self,edgeLabel,fromId):
       edges = self.target.injectEntities(edgeLabel,1,0,1)
       edges[0].get('from').sync(fromId)
       edges[0].get('to').sync(self.get('id'))

class BasePattern:
    def __init__(self,scriptPackage):
        self.scriptPackage = scriptPackage

    def injectInto(self,binding):
        binding.set(self)
        binding.getTarget().retain(binding)

    def injectNeighbors(self,binding):
        pass

    def nextPath(self,path,name):
        return path.get(name)

class Regex(BasePattern):
    def __init__(self,scriptPackage,regex):
        BasePattern.__init__(self,scriptPackage)
        self.regex = regex

    def injectInto(self,binding):
        target = binding.getTarget()
        value = exrex.getone(self.regex)
        target.declareProperty(binding)
        target.constrain(constraint.Choice(),dict(var=binding,values=values.Values([value])),1)
        return BasePattern.injectInto(self,binding)

class Property(BasePattern):
    def __init__(self,scriptPackage,name,descriptor):
        BasePattern.__init__(self,scriptPackage)
        self.name = name
        self.values = descriptor.get('values')
        self.pattern = descriptor.get('semantic')
        self.encoding = descriptor.get('encoding')

    def __str__(self):
        return str(self.__dict__)

    @staticmethod
    def compileProperties(scriptPackage,pathList,descriptors):
        result = []
        for name,descriptor in descriptors.iteritems():
            if 'descriptors' in descriptor:
                result += Property.compileProperties(scriptPackage,pathList+[name],descriptor.get('descriptors'))
            else:
                property = Property(scriptPackage,'.'.join(pathList+[name]),descriptor)
                result.append(property)
        return result

    def makeMetaProperties(self,target,propertyPath,parent):
        if self.pattern:
            for subpatternClass in target.locateTypeDefns(self.pattern):
                subpattern = subpatternClass()
                if hasattr(subpattern,'makeMetaProperties'):
                    subPath = propertyPath
                    if name != None:
                        subPath.append(name)
                    subpattern.makeMetaProperties(target,subPath,parent)
                else:
                    self.makeMetaProperty(target,propertyPath,parent)
        else:
            self.makeMetaProperty(target,propertyPath,parent)

    def makeMetaProperty(self,target,propertyPath,parent):
        subPath = propertyPath
        name = '.'.join(subPath)
        if name not in parent['properties']:
            defn = dict(name=name)
            if self.values:
                defn['values'] = list(self.values)
            if self.pattern:
                defn['semantic'] = self.pattern
            if self.encoding:
                defn['encoding'] = self.encoding
            parent['properties'][name] = defn

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        var = properties

        if self.pattern:
            target.makeEntity(var,self.pattern)
        elif self.values or self.encoding:
            target.declareProperty(binding)
        if self.values:
            target.constrain(constraint.Choice(),dict(var=var,values=values.Values(self.values)),1)
        if not self.pattern:
            target.retain(binding)
        # do not call BasePattern.injectInto(self,binding)

    def collectVariablePaths(self,binding,relPathList,variablePaths):
        if self.pattern:
            patternDefn = binding.getTarget().locateTypeDefn(self.pattern)
            binding.collectVariablePaths_r(patternDefn,binding.getRootPath(),relPathList,variablePaths)
        else:
            variablePaths.append('.'.join(relPathList))

class Object(BasePattern):
    def __init__(self,scriptPackage,propertyDescriptors):
        BasePattern.__init__(self,scriptPackage)
        self.properties = Property.compileProperties(scriptPackage,[],propertyDescriptors)

    def getPropertyNames(self):
        return self.properties.keys()

    def getPropertyPattern(self,propertyName):
        return self.properties.get(propertyName).get('semantic')

    def canEnumerate(self,count):
        return False

    def doEnumerate(self,count):
        raise RuntimeError('Unsupported Method')

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        for propDefn in self.properties:
            propDefn.injectInto(binding.get(propDefn.name))
        return BasePattern.injectInto(self,binding)

    def makeMeta(self,target,patternPath):
        modelKind = 'edge' if 'from' in self.properties and 'to' in self.properties else 'vertex'
        meta = []
        defn = dict(type=modelKind,name=patternPath,properties=dict())
        self.makeMetaProperties(target,[],defn)
        meta.append(defn)
        return meta

    def makeMetaProperties(self,target,propertyPath,defn):
        for property in self.properties:
            property.makeMetaProperties(target,propertyPath,defn)

class Enumeration(BasePattern):
    def __init__(self,scriptPackage,valueList):
        BasePattern.__init__(self,scriptPackage)
        self.valueList = []
        if len(valueList) < 20:
            for value in valueList:
                if value not in self.valueList:
                    self.valueList.append(value)
        else:
            self.valueList = valueList

    def canEnumerate(self,count):
        return count == None or count >= len(self.valueList)

    def doEnumerate(self,count):
        if count == None or count >= len(self.valueList):
            return list(self.valueList)
        else:
            return random.sample(self.valueList,count)

    def injectInto(self,binding):
        target = binding.getTarget()
        propPath = binding.getRootPath()
        target.declareProperty(binding)
        target.constrain(constraint.Choice(),dict(var=propPath,values=values.Values(self.valueList)),1)
        return BasePattern.injectInto(self,binding)

    def makeMeta(self,target,patternPath):
        meta = []
        defn = dict(type='enum',name=patternPath,values=self.doEnumerate(None))
        meta.append(defn)
        return meta


class Trait(BasePattern):
    def __init__(self,scriptPackage,valueList,valueDomains):
        BasePattern.__init__(self,scriptPackage)
        self.valueList = valueList
        self.valueDomains = valueDomains

    def nextPath(self,path,name):
        if name == '@value':
            return path.get(name)
        elif name == '@entry':
            return path.get(name)
        return path.get('@value').get(name)

    def canEnumerate(self,count):
        return count == None or count >= len(self.valueList)

    def doEnumerate(self,count):
        if count == None or count >= len(self.valueList):
            return list(self.valueList)
        else:
            return random.sample(self.valueList,count)

    def makeMeta(self,target,patternPath):
        meta = []
        defn = dict(type='trait',name=patternPath,values=self.doEnumerate(None))
        meta.append(defn)
        return meta

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        target.declareProperty(binding,'@entry')
        #target.setProperty(self,properties.get('@entry'),values.Values(self.valueList))
        target.constrain(constraint.Choice(),dict(var=properties.get('@entry'),values=values.Values(self.valueList)),1)
        if type(self.valueDomains) == dict:
            properties = binding.getRootPath()
            valuePath = properties.get('@value')
            for propName,propDomain in self.valueDomains.iteritems():
                target.declareProperty(binding,'@value.%s' % propName)
                target.constrain(constraint.Lookup(),dict(container=properties.get('@entry'),key=values.Values([propName]),result=valuePath.get(propName)),True)
        return BasePattern.injectInto(self,binding)

    def collectVariablePaths(self,binding,relPathList,variablePaths):
        variablePaths.append('.'.join(relPathList+['@entry']))
        for propName,propDesc in self.valueDomains.iteritems():
            propPath = '.'.join(relPathList+['@value',propName])
            if propPath not in variablePaths:
                variablePaths.append(propPath)

class Vertex(Object):
    def __init__(self,scriptPackage,label,idPattern,propertyDescriptors):
        self.label = label
        self.idPattern = idPattern
        self.propertyDescriptors = propertyDescriptors
        Object.__init__(self,scriptPackage,{
            'label': { 'values': [label] },
            'id': { 'name': 'id', 'semantic': idPattern },
            'properties': { 'descriptors': propertyDescriptors }
        })

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        result = Object.injectInto(self,binding)
        target.constrain(constraint.Choice(),dict(var=properties.get('label'),values=values.Values([self.label])))
        binding.get('id').get('label').sync(binding.get('label'))
        return result

    def makeMeta(self,target,patternPath):
        meta = []
        defn = dict(type='vertex',name=patternPath,properties=dict())
        self.makeMetaProperties(target,[],defn)
        meta.append(defn)
        return meta

class Edge(Object):
    def __init__(self,scriptPackage,label,idPattern,propertyDescriptors):
        self.label = label
        self.idPattern = idPattern
        self.propertyDescriptors = propertyDescriptors
        Object.__init__(self,scriptPackage,{
            'label': { 'values': [label] },
            'from': { 'name': 'from', 'semantic': idPattern },
            'to': { 'name': 'to', 'semantic': idPattern },
            'properties': { 'descriptors': propertyDescriptors}
        })

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        result = Object.injectInto(self,binding)
        target.constrain(constraint.Choice(),dict(var=properties.get('label'),values=values.Values([self.label])))
        return result

    def makeMeta(self,target,patternPath):
        meta = []
        defn = dict(type='edge',name=patternPath,properties=dict())
        self.makeMetaProperties(target,[],defn)
        meta.append(defn)
        return meta

class Collection(BasePattern):
    def __init__(self,itemPattern,count):
        BasePattern.__init__(self,'.collection')
        self.itemPattern = itemPattern
        self.count = count
        self.owners = []
        self.reuseWeight = None
        self.spawnWeight = None

    def addOwner(self,owner,reuseWeight=None,spawnWeight=None):
        if type(owner) in (tuple,list):
            self.owners += owner
        else:
            self.owners.append(owner)
        self.reuseWeight = reuseWeight
        self.spawnWeight = spawnWeight

    def getPropertyNames(self):
        return []

    def canEnumerate(self,count):
        return False

    def doEnumerate(self,count):
        raise RuntimeError('Unsupported Method')

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        countValue = random.choice(self.count)

        reuseWeight = self.reuseWeight or  0
        spawnWeight = self.spawnWeight or 1

        target.trace('%s SPAWN %d %s' % (str(properties),countValue,self.itemPattern))
        self.injectedItems = target.injectEntities(self.itemPattern,countValue,reuseWeight,spawnWeight)
        return BasePattern.injectInto(self,binding)

    def getInjectedItems(self):
        return self.injectedItems

    def makeEdgeFromId(self,edgeLabel,fromId):
        target = fromId.getTarget()
        for member in self.injectedItems:
            edges = target.injectEntities(edgeLabel,1,0,1)
            edges[0].get('from').sync(fromId)
            edges[0].get('to').sync(member.get('id'))

    def makeEdgeToId(self,edgeLabel,toId):
        target = toId.getTarget()
        for member in self.injectedItems:
            edges = target.injectEntities(edgeLabel,1,0,1)
            memberId = member.get('id')
            edges[0].get('from').sync(memberId)
            edges[0].get('to').sync(toId)

    def makePropertyIndex(self,suffix,index=0):
        for member in self.injectedItems:
            indexProperty = member.get(suffix).getRootPath()
            member.getTarget().declareProperty(member,suffix)
            member.getTarget().setProperty(self,indexProperty,index)
            index += 1

    def syncEachProperty(self,memberProp,otherProp):
        target = otherProp.getTarget()
        for member in self.injectedItems:
            target.constrain(constraint.Equals(),dict(a=member.get(memberProp),b=otherProp))
