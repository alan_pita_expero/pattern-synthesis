class Alias(packages.patterns.Vertex):
    global familyNames
    global givenNames

    def __init__(self):
        packages.patterns.Vertex.__init__(self,scriptPackage,'person.alias','datacube.vertex.id',{
            'family': { 'name': 'family', 'semantic': 'lorem.mutator' },
            'given': { 'name': 'given', 'semantic': 'lorem.mutator' },
            #'full': { 'encoding': { 'java': 'java.lang.String' } }
        })

exports = [Alias]
