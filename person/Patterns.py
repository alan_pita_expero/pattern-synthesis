import random


class Person(packages.patterns.Vertex):
    def __init__(self):
        packages.patterns.Vertex.__init__(self,scriptPackage,'Person','datacube.vertex.id',{
            'alienNum': { 'values': xrange(10000,20000) },
            'dob': { 'name': 'dob', 'semantic': 'person.event.birth' },
            'name': { 'name': 'name', 'semantic': 'person.name' },
            'eyeColor': { 'name': 'eyeColor', 'semantic': 'person.biometric.eyeColor' },
            'fingerprintId': { 'name': 'fingerprintId', 'semantic': 'person.biometric.fingerprintId' },
            'gender': { 'name': 'gender', 'semantic': 'person.biometric.gender' },
            'hairColor': { 'name': 'hairColor', 'semantic': 'person.biometric.hairColor' },
            'height': { 'name': 'height', 'semantic': 'person.biometric.height' },
            'weight': { 'name': 'weight', 'semantic': 'person.biometric.weight' },
            'citizenship': { 'name': 'citizenship', 'semantic': 'place.country' },
            'email1': { 'name': 'email1', 'semantic': 'contact.email'},
            'email2': { 'name': 'email2', 'semantic': 'contact.email'},
            'phone1': { 'name': 'phone1', 'semantic': 'contact.phone'},
            'phone2': { 'name': 'phone2', 'semantic': 'contact.phone'}
        })


exports = [Person]
