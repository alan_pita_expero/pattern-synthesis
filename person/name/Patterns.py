# -*- coding: utf-8 -*-

class Name(packages.patterns.Object):
    global Phrase3Formatter
    global familyNames
    global givenNames

    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'family': { 'semantic': 'person.name.family' },
            'given': { 'semantic': 'person.name.given' }
        })

exports = [Name]
