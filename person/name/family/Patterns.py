# -*- coding: utf-8 -*-
fp = open('%s../namedb.json' % (scriptPath))
db = packages.json.load(fp)

familyNames = [ sample.get('familyName') for sample in db if 'familyName' in sample ]

class Phrase3Formatter:
    def __str__(self):
      return 'phrase_formatter'

    def project(self,constraint,args,target):
        count   = constraint.getArgument('count',target)
        output  = constraint.getArgument('output',target)
        inputNames = [ int(argName) for argName in constraint.getArgumentNames() if argName not in ('count','output') ]
        inputNames.sort()
        inputs = [ constraint.getArgument(str(argName),target) for argName in inputNames ]

        if args != ['output']:
            if count.isSingle():
                nonSingle = [ input for input in inputs[:int(count.single())] if not input.isSingle() ]
                if len(nonSingle) == 0:
                    inputs = inputs[:count.single()]
                    value = ''.join([input.single() for input in inputs])
                    constraint.setArgument('output',target,packages.values.Values([value]))
                    return packages.constraint.REDUCE
        return packages.constraint.CONTINUE

class Name(packages.patterns.Object):
    global Phrase3Formatter
    global familyNames

    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'wordCount': { 'values': [1,1,1,1,1,1,1,1,1,3,3,5], 'encoding': { 'java': 'java.lang.Integer' } },
            'word1': { 'values': familyNames, 'encoding': { 'java': 'java.lang.String' } },
            'wordSep1': { 'values': ['-',' '], 'encoding': { 'java': 'java.lang.String' } },
            'word2': { 'values': familyNames, 'encoding': { 'java': 'java.lang.String' } },
            'wordSep2': { 'values': ['-',' '], 'encoding': { 'java': 'java.lang.String' } },
            'word3': { 'values': familyNames, 'encoding': { 'java': 'java.lang.String' } },
            'phrase': { 'encoding': { 'java': 'java.lang.String' } },
        })

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.constrain(Phrase3Formatter(),{
            'count': binding.rootPath.get('wordCount'),
            '1':     binding.rootPath.get('word1'),
            '2':     binding.rootPath.get('wordSep1'),
            '3':     binding.rootPath.get('word2'),
            '4':     binding.rootPath.get('wordSep2'),
            '5':     binding.rootPath.get('word3'),
            'output':binding.rootPath.get('phrase')
        })

exports = [Name]
