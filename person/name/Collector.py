#!/usr/bin/env python

"""Generate a database of person names weighted by approximate prevalence in an normal population using a CSV file hosted by drupal.org

To download the database:
curl -X GET https://ftp.drupal.org/files/projects/namedb-7.x-1.0-beta2.tar.gz > namedb-7.x-1.0-beta2.tar.gz
tar xvfz namedb-7.x-1.0-beta2.tar.gz
namedb.py namedb/data/data.dat > data/synthetic/sample/namedb.json

To cleanup artifacts
rm -rf namedb-7.x-1.0-beta2.tar.gz namedb
"""

import sys
import csv
import json
import os
import subprocess

scriptPath = os.path.dirname(os.path.realpath(sys.argv[0]))

proc = subprocess.check_output("""curl -s -N https://ftp.drupal.org/files/projects/namedb-7.x-1.0-beta2.tar.gz | tar -Oxz namedb/data/data.dat""",shell=True)
samples = csv.reader(proc.splitlines())

data = []

index = 0
maxWeight = None
for row in samples:
	rank = int(row[5])
	if rank < 500:
		weight = int(1000000 * pow(0.99,rank))
		if int(row[3]) > 0:
			data.append({ 'familyName': row[0], 'weight': weight })
		if int(row[4]) > 0:
			data.append({ 'givenName': row[0], 'weight': weight })
		index += 1

print json.dumps(data,indent=2)
