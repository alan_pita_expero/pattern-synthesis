import random
import patterns

# see http://www.vsp.state.va.us/afis/livescan/files/VAEye&HairColor&RaceCodes.pdf

colors = """Male
Female
Other""".split('\n')

class Gender(packages.patterns.Enumeration):
    global colors

    def __init__(self):
        packages.patterns.Enumeration.__init__(self,scriptPackage,colors)

exports = [Gender]
