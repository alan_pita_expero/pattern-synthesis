import uuid

class Any(packages.patterns.Regex):
    global uuid

    def __init__(self):
        packages.patterns.Regex.__init__(self,scriptPackage,r'[0-9a-f]{9}')

    def canEnumerate(self,count):
        return False

    def doEnumerate(self,count):
        raise RuntimeError('Unsupported Method')

exports = [Any]
