import random

# see http://www.vsp.state.va.us/afis/livescan/files/VAEye&HairColor&RaceCodes.pdf

colors = [
{ 'code': 'BAL', 'en_US': 'Bald' },
{ 'code': 'BLK', 'en_US': 'Black' },
{ 'code': 'BLN', 'en_US': 'Blond' },
{ 'code': 'BLU', 'en_US': 'Blue' },
{ 'code': 'BRO', 'en_US': 'Brown' },
{ 'code': 'GRY', 'en_US': 'Gray' },
{ 'code': 'GRN', 'en_US': 'Green' },
{ 'code': 'ONG', 'en_US': 'Orange' },
{ 'code': 'PNK', 'en_US': 'Pink' },
{ 'code': 'PLE', 'en_US': 'Purple' },
{ 'code': 'RED', 'en_US': 'Red' }
]

class HairColors(packages.patterns.Enumeration):
    global colors
    def __init__(self):
        packages.patterns.Enumeration.__init__(self,scriptPackage,colors)

exports = [HairColors]
