import random
import patterns

# see http://www.vsp.state.va.us/afis/livescan/files/VAEye&HairColor&RaceCodes.pdf

colors = [
{'code': 'BLU', 'en_US': 'Blue'},
{'code': 'BRO', 'en_US': 'Brown'},
{'code': 'GRY', 'en_US': 'Gray'},
{'code': 'GRN', 'en_US': 'Green'},
{'code': 'HAZ', 'en_US': 'Hazel'},
{'code': 'MAR', 'en_US': 'Maroon'},
{'code': 'MUL', 'en_US': 'Multiple'},
{'code': 'PNK', 'en_US': 'Pink'},
{'code': 'XXX', 'en_US': 'Unknown'}
]

class EyeColors(packages.patterns.Enumeration):
    global colors

    def __init__(self):
        packages.patterns.Enumeration.__init__(self,scriptPackage,colors)

exports = [EyeColors]
