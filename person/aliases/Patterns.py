class Aliases(packages.patterns.Vertex):
    def __init__(self):
        packages.patterns.Vertex.__init__(self,scriptPackage,'aliases','datacube.vertex.id',{})

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.setProperty(self,properties.get('id').get('label'),['aliases'])

    def injectNeighbors(self,binding):
        contPrimary = packages.patterns.Collection('person',[1])

        contPrimary.addOwner(self,0,1) # 100% owned
        contPrimary.injectInto(binding.get('person'))
        contPrimary.makeEdgeFromId('person.hasPrimary',binding.get('id'))
        primary = contPrimary.getInjectedItems()[0]

        contAliases = packages.patterns.Collection('person.alias',xrange(1,4))

        contAliases.addOwner(self,0,1) # 100% owned
        contAliases.injectInto(binding.get('aliases'))
        contAliases.makeEdgeFromId('person.hasAlias',binding.get('id'))

        for alias in contAliases.getInjectedItems():
            target.constrain(packages.constraint.Equals(),dict(a=alias.get('properties.family.plain'),b=primary.get('properties.name.family')))
            target.constrain(packages.constraint.Equals(),dict(a=alias.get('properties.given.plain'),b=primary.get('properties.name.given')))

exports = [Aliases]
