maxDaysAgo = 365*83

class Any(packages.patterns.Object):
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'birthDate'     : { 'name': 'birthDate', 'semantic': 'time.moment' }
        })
    global maxDaysAgo

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)

        daysAgo = packages.random.choice(xrange(0,maxDaysAgo))
        dob = packages.arrow.now()
        dob.replace(hour=0,minute=0,second=0,microsecond=0)
        dob -= packages.datetime.timedelta(days=daysAgo)
        asISO = dob.format('YYYY-MM-DDTHH:mm:ssZZ')
        target.constrain(packages.constraint.Choice(),dict(var=properties.get('birthDate').get('iso'),values=packages.values.Values([asISO])))

exports = [Any]
