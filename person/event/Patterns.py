eventTypes = [ 'Birth', 'Marriage', 'Divorce', 'Gain', 'Loss', 'Relocation', 'Travel', 'Incarceration', 'Hospitalization', 'Death' ]

class LifecycleEvent(packages.patterns.Vertex):
    global eventTypes

    def __init__(self):
        packages.patterns.Vertex.__init__(self,scriptPackage,scriptPackage,'datacube.vertex.id',{
            'type': { 'values': eventTypes },
            'period': { 'name': 'period', 'semantic': 'time.period' },
        })

exports = [LifecycleEvent]
