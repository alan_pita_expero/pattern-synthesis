def geometric(value,factor,prob):
    if random.uniform() >= prob:
        return value
    return geometric(value * factor,factor,prob)

class EventTimestamp(packages.patterns.Object):
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'start': { 'name': 'start', 'semantic': 'time.moment' },
            'end':   { 'name': 'end', 'semantic': 'time.moment' },
            'date':  { 'name': 'date', 'semantic': 'time.moment' }
        })

class PastHour(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        hoursAgo = int(geometric(1,2,0.5))
        date = datetime.date.today()
        start = datetime.datetime.now()
        start.replace(minute=0,second=0,millisecond=0)
        start -= datetime.timedelta(hours=hoursAgo)
        end = start + datetime.timedelta(1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class PastDay(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        daysAgo = int(geometric(1,2,0.5))
        date = datetime.date.today() - datetime.timedelta(days=daysAgo)
        start = datetime.datetime.today() - datetime.timedelta(days=daysAgo)
        end = start + datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class PastWeek(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        weeksAgo = int(geometric(1,2,0.5))
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.weekday()+7*weeksAgo)
        start = date.datetime()
        end = start + datetime.timedelta(7) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class PastMonth(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        monthsAgo = int(geometric(1,2,0.5))
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        date -= datetime.timedelta(months=monthsAgo)
        start = date.datetime()
        end = start + datetime.timedelta(months=1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class PastYear(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        yearsAgo = int(geometric(1,2,0.5))
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        date.replace(month=1,day=1)
        date -= datetime.timedelta(years=yearsAgo)
        start = date.datetime()
        end = start + datetime.timedelta(years=1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class LastHour(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        start = datetime.datetime.now()
        start.replace(minute=0,second=0,millisecond=0)
        start -= datetime.timedelta(hours=1)
        end = start + datetime.timedelta(1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class Yesterday(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today() - datetime.timedelta(days=1)
        start = datetime.datetime.today() - datetime.timedelta(days=1)
        end = start + datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class LastWeek(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.weekday()+7)
        start = date.datetime()
        end = start + datetime.timedelta(7) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class LastMonth(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        date -= datetime.timedelta(months=1)
        start = date.datetime()
        end = start + datetime.timedelta(months=1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class LastYear(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        date.replace(month=1,day=1)
        date -= datetime.timedelta(years=1)
        start = date.datetime()
        end = start + datetime.timedelta(years=1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class ThisHour(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        start = datetime.datetime.now()
        start.replace(minute=0,second=0,millisecond=0)
        end = start + datetime.timedelta(1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class Today(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        start = datetime.datetime.today()
        end = start + datetime.timedelta(1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class ThisWeek(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.weekday())
        start = date.datetime()
        end = start + datetime.timedelta(7) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class ThisMonth(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        start = date.datetime()
        end = start + datetime.timedelta(months=1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class ThisYear(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        date.replace(month=1,day=1)
        start = date.datetime()
        end = start + datetime.timedelta(years=1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class NextHour(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        start = datetime.datetime.now()
        start.replace(minute=0,second=0,millisecond=0)
        start += datetime.timedelta(hours=1)
        end = start + datetime.timedelta(1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class Tomorrow(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today() + datetime.timedelta(days=1)
        start = datetime.datetime.today() + datetime.timedelta(days=1)
        end = start + datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class NextWeek(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        date += datetime.timedelta(days=date.weekday()+7)
        start = date.datetime()
        end = start + datetime.timedelta(7) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class NextMonth(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        monthsAgo = int(geometric(1,2,0.5))
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        date += datetime.timedelta(months=1)
        start = date.datetime()
        end = start + datetime.timedelta(months=monthsAgo) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class NextYear(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        date.replace(month=1,day=1)
        date += datetime.timedelta(years=1)
        start = date.datetime()
        end = start + datetime.timedelta(years=1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class FutureHour(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        hoursAgo = int(geometric(1,2,0.5))
        date = datetime.date.today()
        start = datetime.datetime.now()
        start.replace(minute=0,second=0,millisecond=0)
        start += datetime.timedelta(hours=hoursAgo)
        end = start + datetime.timedelta(1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class FutureDay(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        daysAgo = int(geometric(1,2,0.5))
        date = datetime.date.today() + datetime.timedelta(days=daysAgo)
        start = datetime.datetime.today() + datetime.timedelta(days=daysAgo)
        end = start + datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class FutureWeek(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        weeksAgo = int(geometric(1,2,0.5))
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.weekday())
        date += datetime.timedelta(days=7*weeksAgo)
        start = date.datetime()
        end = start + datetime.timedelta(7) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class FutureMonth(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        monthsAgo = int(geometric(1,2,0.5))
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        date += datetime.timedelta(months=monthsAgo)
        start = date.datetime()
        end = start + datetime.timedelta(months=1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

class FutureYear(EventTimestamp):
    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        yearsAgo = int(geometric(1,2,0.5))
        date = datetime.date.today()
        date -= datetime.timedelta(days=date.day)
        date.replace(month=1,day=1)
        date += datetime.timedelta(years=yearsAgo)
        start = date.datetime()
        end = start + datetime.timedelta(years=1) - datetime.timedelta(0,1)
        if target.hasProperty(properties.get('start')):
            target.setProperty(self,properties.get('start'),start)
        if target.hasProperty(properties.get('end')):
            target.setProperty(self,properties.get('end'),end)
        if target.hasProperty(properties.get('date')):
            target.setProperty(self,properties.get('date'),date)

exports = [PastHour,PastDay,PastWeek,PastMonth,PastYear,LastHour,Yesterday,LastWeek,LastMonth,LastYear,ThisHour,Today,ThisWeek,ThisMonth,ThisYear,NextHour,Tomorrow,NextWeek,NextMonth,NextYear,FutureHour,FutureDay,FutureWeek,FutureMonth,FutureYear]
