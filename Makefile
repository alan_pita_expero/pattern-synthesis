COUNT=1000
#AWS_LAMBDA=arn:aws:lambda:us-east-1:375183975009:function:datacube-pattern-gen
THREADS=1

gen-journey:
	./generate.py --modelKind vertex --modelType datacube.journey --count $(COUNT) --threads $(THREADS) #--awsLambda $(AWS_LAMBDA)

trace-journey:
		./generate.py --modelKind vertex --modelType datacube.journey --count $(COUNT) --threads $(THREADS) --trace > trace-journey.out

clean:
	rm -rf *.json *.csv
