import os
import random
import constraint
import datetime
import arrow
import pytz
import tzlocal
import values
import patterns
import variable
import json
import xeger
import re
import library

def set_eq(a,b):
    if type(a) not in (list,tuple):
        return set_eq([a],b)
    elif type(b) not in (list,tuple):
        return set_eq(a,[b])

    x = a[:]
    y = b[:]
    x.sort()
    y.sort()
    return x == y

class ProfileCounters:
    def __init__(self):
        self.startUsec = None
        self.kindToNameToNumChoices = dict()
        self.kindToNameToNumChanges = dict()

    def start(self,usec):
        self.startUsec = usec
        self.kindToNameToNumChoices = dict()
        self.kindToNameToNumChanges = dict()

    def report(self):
        results = []
        for kind in self.kindToNameToNumChanges.keys():
            for name in self.kindToNameToNumChanges.get(kind).keys():
                count = self.kindToNameToNumChanges.get(kind).get(name)
                results.append(dict(action='CHANGE',kind=kind,name=name,count=count))

        for kind in self.kindToNameToNumChoices.keys():
            for name in self.kindToNameToNumChoices.get(kind).keys():
                count = self.kindToNameToNumChoices.get(kind).get(name)
                results.append(dict(action='CHOICE',kind=kind,name=name,count=count))

        results.sort(key=lambda x : (x['action'],x['kind'],x['count'],x['name']))
        return 'BEGIN SOLVING PROFILE'+'\n'.join(['%(action)s | %(kind)s | %(name)s | %(count)d' % item for item in results])+'\nEND   SOLVING PROFILE'

    def countChoice(self,table):
        for kind in table.keys():
            if kind not in self.kindToNameToNumChoices:
                self.kindToNameToNumChoices[kind] = dict()
            for name in table.get(kind):
                if name not in self.kindToNameToNumChoices[kind]:
                    self.kindToNameToNumChoices[kind][name] = 0
                self.kindToNameToNumChoices[kind][name] += 1

    def countChange(self,table):
        for kind in table.keys():
            if kind not in self.kindToNameToNumChanges:
                self.kindToNameToNumChanges[kind] = dict()
            for name in table.get(kind):
                if name not in self.kindToNameToNumChanges[kind]:
                    self.kindToNameToNumChanges[kind][name] = 0
                self.kindToNameToNumChanges[kind][name] += 1

class Queue:
    def __init__(self):
        self.constraints = []
        self.paths = dict()

    def clear(self):
        self.constraints = []
        self.paths = dict()

    def count(self):
        return len(self.constraints)

    def isBusy(self):
        return self.count() > 0

    def enqueue(self,constraint,path):
        if constraint not in self.constraints:
            self.constraints.append(constraint)
        if constraint not in self.paths:
            self.paths[constraint] = []
        self.paths[constraint].append(path)

    def process(self,target):
        chosen = self.constraints[0]
        self.constraints = self.constraints[1:]
        paths = self.paths.get(chosen)
        before = len(self.constraints)
        if not chosen.isMandatory():
            target.beforeNonMandatory()
        if chosen.project([target.lookup(path) for path in paths],target) == constraint.AGAIN:
            self.constraints.append(chosen)
        if not chosen.isMandatory():
            target.afterNonMandatory()
        after = len(self.constraints)
        target.trace("Queued %d constraints after %s" % (after-before,chosen))

class Target:
    def __init__(self,library,patternsByType=None):
      self.pathToPropertyValues = dict()
      self.pathToBinding = dict()
      self.constraintByPath = dict()
      self.patternsByType = patternsByType
      self.bindingByPathString = dict()
      self.unresolved = []
      self.reductionThreshold = 16
      self.requireQueue = Queue()
      self.selectQueue = Queue()
      self.biasQueue = Queue()
      self.traceMode = False
      self.library = library
      self.rootBinding = patterns.Binding(self,variable.Path([]))
      self.lastChangeIndex = 0
      self.startTime = None
      self.changeMandatory = True
      self.profileCounters = ProfileCounters()

    def commit(self):
      for path,pattern in self.bindingByPathString.iteritems():
          if binding.pattern:
              self.patternsByType[binding.pattern.patternPackage] = binding.capture()

    def setTraceMode(self):
        self.traceMode = True

    def getSolvingUsec(self):
        if not self.startTime:
            return 0

        usec = datetime.datetime.now() - self.startTime
        usec = int(usec.total_seconds() * 1000000)
        return usec

    def trace(self,message):
      if self.traceMode:
          traceTime = ''
          if self.startTime:
              usec = self.getSolvingUsec()
              traceTime = '%d usec : ' % usec
          for line in message.split('\n'):
              print ("%s%s" % (traceTime,line)).encode('utf-8')

    def seed(self,seed=None):
      random.seed(seed)

    def lookup(self,path):
        if type(path) in (unicode,str):
            return self.lookup(path.split('.'))
        else:
            return variable.Path(path)

    def hasProperty(self,property):
        if isinstance(property,patterns.Binding):
            property = property.getRootPath()
        if isinstance(property,variable.Path):
            property = property.value()
        return self.has(property)

    def getProperty(self,property):
        if property == None:
            raise RuntimeError('invalid property')
        if isinstance(property,patterns.Binding):
            property = property.getRootPath()
        if isinstance(property,variable.Path):
            property = property.value()
        return self.read(property,property)

    def detectResolution(self,property,newValue):
        if not isinstance(newValue,values.Values):
            raise RuntimeError('Attempt to set %s with value %s' % (property,newValue))
        resolved = newValue.isSingle()
        if type(property) in (list,tuple):
            path = '.'.join(property)
        else:
            path = str(property)
        if not resolved and path not in self.unresolved:
            self.unresolved.append(path)
        elif resolved and path in self.unresolved:
            self.unresolved.remove(path)
        if resolved:
            self.trace('RESOLVED %s' % property)

    def setProperty(self,who,property,newValue):
        if isinstance(property,patterns.Binding):
            property = property.getRootPath()
        if isinstance(property,variable.Path):
            property = property.value()
        if newValue == None:
            raise RuntimeError('None no longer represents the universal set')
        elif isinstance(newValue,values.Values):
            pass
        elif type(newValue) in (list,tuple):
            newValue = values.Values(newValue)
        else:
            newValue = values.Values([newValue])
        prevValue = None
        if self.has(property):
            prevValue = self.read(property,property)
        if prevValue != None:
            newValue = newValue.intersect(prevValue)
            if prevValue.equals(newValue):
                return False
        self.detectResolution(property,newValue)

        path = '.'.join([str(item) for item in property])
        self.trace('CHANGE: @%d setProperty %s' % (self.lastChangeIndex,who))
        self.trace('        REDUCE %s' % path)
        self.trace('        BEFORE %s' % (prevValue))
        self.trace('        AFTER  %s' % (newValue))
        self.modify(property,newValue)

        changeDesc = dict()
        changeDesc['var'] = dict()
        changeDesc['var'][str(property)] = 1
        changeDesc['who'] = dict()
        changeDesc['who'][str(who)] = 1
        if self.changeMandatory:
            self.profileCounters.countChange(changeDesc)
        else:
            self.profileCounters.countChoice(changeDesc)

        #self.trace('        unresolved = %s' % self.unresolved)
        #    print 'BEFORE: setProperty(%s <= %s) %s' % ('.'.join(property),newValue,self.data)
        #    print 'AFTER : setProperty(%s <= %s) %s' % ('.'.join(property),newValue,self.data)

        remainder = property[:]
        while len(remainder) > 0:
            path = '.'.join([str(item) for item in remainder])
            activated = self.constraintByPath.get(path) or []
            self.trace('        |activated| = %d (%s)' % (len(activated),path))
            for constraint in activated:
                self.activate(constraint,path)
            remainder.pop()

    def has(self,path):
        return '.'.join(path) in self.pathToPropertyValues

    def read(self,original,path):
        pathStr = '.'.join(path)
        if pathStr not in self.pathToPropertyValues:
            raise RuntimeError('Path was not declared %s\n%s' % (pathStr,'\n'.join(self.pathToPropertyValues.keys())))
        return self.pathToPropertyValues.get(pathStr).getValues()

    def modify(self,path,newValue):
        pathStr = '.'.join(path)
        if pathStr not in self.pathToPropertyValues:
            raise RuntimeError('Cannot write property %s\n%s' % (pathStr,'\n'.join(self.pathToPropertyValues.keys())))
        self.pathToPropertyValues.get(pathStr).setValues(newValue,self.lastChangeIndex)
        self.lastChangeIndex += 1

    def locateTypeDefn(self,typeName):
        return self.library.locate(typeName)

    def declareProperty(self,binding,sub = None):
        varPath = None
        if sub != None:
            varPath = binding.get(sub).getRootPath()
        else:
            varPath = binding.getRootPath()
        pathStr = str(varPath)
        if pathStr in self.pathToPropertyValues:
            pass
            #raise RuntimeError('Path is already declared %s' % pathStr)
        else:
            self.trace('DECLARE %s' % pathStr)
            self.pathToPropertyValues[pathStr] = variable.Variable(varPath,values.Values())
            self.pathToBinding[pathStr] = binding

    def makeEntity(self,symbol,spec):
        if type(symbol) in (str,unicode):
            propertyPath = symbol.split('.')
        elif type(symbol) in (list,tuple):
            propertyPath = symbol
        elif isinstance(symbol,variable.Path):
            propertyPath = symbol.value()
        else:
            raise RuntimeError('Argument symbol must be of type str,unicode, or Path instead of %s' % type(symbol))
        symbol = '.'.join(propertyPath)

        if type(spec) in (unicode,str):
            typeDefns = self.library.locate(spec)
        else:
            typeDefns = [spec]
        if typeDefns != None:
            self.trace('%s had %d definitions for entity %s' % (spec,len(typeDefns),symbol))
            typeDefn = random.choice(typeDefns)
            result = self.bindAndInjectInto(propertyPath,typeDefn)
        else:
            self.trace('constructing definition of %s for entity %s' % (spec,symbol))
            defnNames = self.library.getPackageNames()
            defnNames.sort()
            compositePropertyPath = dict()
            for defnPath in defnNames:
                if defnPath.startswith(spec):
                    suffixPath = defnPath[len(spec)+1:].split('.')
                    subPath = propertyPath+suffixPath
                    if not self.hasProperty(subPath):
                        self.trace('Generating %s' % (subPath))
                        subTypeDefn = self.chooseType(defnPath)
                        compositePropertyPath['.'.join(suffixPath)] = dict(instance=self.bindAndInjectInto(subPath,subTypeDefn))
            result = patterns.Object(self,compositePropertyPath)
        return result

    def chooseType(self,defnPath):
        typeDefns = self.library.locate(defnPath)
        if typeDefns == None or len(typeDefns) == 0:
            raise RuntimeError('Unable to locate type %s' % defnPath)
        typeDefn = random.choice(typeDefns)

    def bindAndInjectInto(self,propertyPath,typeDefn):
        pathString = '.'.join(propertyPath)
        objectPath = self.lookup(pathString)
        binding = self.rootBinding.get(pathString)
        binding.set(typeDefn)
        typeDefn.injectInto(binding)
        typeDefn.injectNeighbors(binding)
        return binding

    def retain(self,binding):
        pathString = str(binding.getRootPath())
        self.trace('RETAIN %s %s' % (pathString,binding.pattern))
        self.bindingByPathString[pathString] = binding

    def getPattern(self,property):
        path = str(property)
        if path not in self.bindingByPathString:
            raise RuntimeError('Cannot locate pattern by path %s\n%s' % (property,'\n'.join(self.bindingByPathString.keys())))
        return self.bindingByPathString.get(path)

    def toDict(self,asSingle=False):
        result = dict()
        for pathStr,property in self.pathToPropertyValues.iteritems():
            property.toDict(result,asSingle)
        return result

    def constrain(self,function,binding,priority=True):
        useBinding = dict()
        for (var,path) in binding.iteritems():
            if isinstance(path,patterns.Binding):
                useBinding[var] = path.getRootPath()
            else:
                useBinding[var] = binding[var]

        theConstraint = constraint.Constraint(function,useBinding,priority)
        self.trace('ATTACH %s' % theConstraint)
        for (var,path) in useBinding.iteritems():
            if isinstance(path,variable.Path):
                pathStr = str(path)
                self.constraintByPath[pathStr] = self.constraintByPath.get(pathStr) or []
                self.constraintByPath[pathStr].append(theConstraint)
                if not self.has(pathStr.split('.')):
                    raise RuntimeError('Attachment references undefined variable %s' % pathStr)
                self.detectResolution(path,values.Values())
            elif isinstance(path,values.Values):
                pass
            elif path == None:
                pass
            else:
                raise RuntimeError('Argument %s should be of type Path or Values but instead is of type %s' % (var,type(path)))

    def injectEntities(self,spec,count=1,reuseWeight=1,spawnWeight=1):
        result = []
        for index in xrange(0,count):
            candidates = []
            if type(spec) in (unicode,str):
                self.trace('self.patternsByType.get(%s) = %s' % (spec,self.patternsByType.get(spec)))
                candidates = [candidate for candidate in (self.patternsByType.get(spec) or []) if candidate not in result]
            code = random.choice(xrange(0,reuseWeight+spawnWeight))
            if reuseWeight > 0:
                self.trace('Possible REUSE, seeking %s with %d candidates' % (spec,len(candidates)))
            if len(candidates)>0 and code < reuseWeight:
                self.trace('Injecting %s via REUSE' % spec)
                capture = random.choice(candidates)
                binding = capture.rebind(self)
                result.append(binding)
            else:
                self.trace('Injecting %s via SPAWN' % spec)
                index = 0
                prefix = str(spec)
                if prefix.startswith('__builtin__.'):
                    prefix = prefix[12:]
                while '%s.%d' % (prefix,index) in self.bindingByPathString:
                    index += 1
                entityPath = '%s.%d' % (prefix,index)
                result.append(self.makeEntity(entityPath,spec))
        return result

    def activate(self,constraint,path):
        if constraint.isMandatory():
            self.requireQueue.enqueue(constraint,path)
        elif not constraint.isSelect():
            self.selectQueue.enqueue(constraint,path)
        else:
            self.biasQueue.enqueue(constraint,path)

    def close(self):
        self.startTime = datetime.datetime.now()
        self.requireQueue.clear()
        self.selectQueue.clear()
        self.biasQueue.clear()

        for (path,constraintList) in self.constraintByPath.iteritems():
            for chosen in constraintList:
                self.activate(chosen,path)
        self.trace('Queue primed with %d mandatory constraints' % self.requireQueue.count())

        while self.requireQueue.isBusy() or self.selectQueue.isBusy() or self.biasQueue.isBusy() or len(self.unresolved) > 0:
            self.trace("BEGIN solving round |require| = %d |select| = %d |bias| = %d |unresolved| = %d" %
                (self.requireQueue.count(),self.selectQueue.count(),self.biasQueue.count(),len(self.unresolved)))

            while self.requireQueue.isBusy():
                self.requireQueue.process(self)

            while self.selectQueue.isBusy() and not self.requireQueue.isBusy():
                self.selectQueue.process(self)

            while self.biasQueue.isBusy() and not self.selectQueue.isBusy() and not self.requireQueue.isBusy():
                self.biasQueue.process(self)

            self.trace("|unresolved| = %d" % len(self.unresolved))
            while self.unresolved != None and len(self.unresolved) > 0 and not self.biasQueue.isBusy() and not self.selectQueue.isBusy() and not self.requireQueue.isBusy():
                path = random.choice(self.unresolved)
                self.trace('RANDOMIZE %s %s' % (path,self.getProperty(path.split('.'))))
                self.unresolved.remove(path)
                pathList = path.split('.')
                self.beforeNonMandatory()
                newValues = self.getProperty(pathList).reduce(path,self)
                if newValues:
                    self.setProperty("random reduction",pathList,newValues)
                self.afterNonMandatory()

    def beforeNonMandatory(self):
        self.trace(self.profileCounters.report())
        self.profileCounters.start(self.getSolvingUsec())
        self.changeMandatory = False

    def afterNonMandatory(self):
        self.changeMandatory = True

    def writeGenericJSON(self,fileName):
        data = self.toDict(True)
        fp = open(fileName,'wt')
        if not fp:
            raise RuntimeError('Cannot write file')

        fp.write(json.dumps(data,indent=2))
        fp.close()
