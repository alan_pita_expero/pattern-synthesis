#!/usr/bin/env python

import argparse
import boto3
import botocore
import os
import sys
import json
import datetime
import library
import target
import threading
import cProfile
import traceback

scriptPath = os.path.dirname(os.path.realpath(__file__))
if 'GRAPH_FACTORY' in os.environ:
    sys.path.append('%s/python' % os.environ.get('GRAPH_FACTORY'))

libPath = scriptPath

import target
#from core import task
#from core import process
#from core import Utility
#from core.MetaSchema import *
#from core.SemanticType import *
#from core.Repository import *
#from core.configurable import *

class Generator:
    def __init__(self):
        parser = argparse.ArgumentParser(description='Generate Synthetic Journey data patterns')
        parser.add_argument('--modelType',help='definition name to generate')
        parser.add_argument('--modelKind',help='definition type (vertex/edge/view/shape) to generate')
        parser.add_argument('--trace',help='print detailed log messages about solving',default=False,action='store_true')
        parser.add_argument('--count',nargs='?',help='Number of vertices/edges to generate',default=1)
        parser.add_argument('--awsLambda',help='AWS Lambda function to invoke instead of running locally',default=None)
        parser.add_argument('--threads',help='Number of threads to execute in parallel',default=None)
        self.args = parser.parse_args()
        self.retained = dict()
        self.timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        self.threads = []
        self.resultIndex = 0
        if self.args.awsLambda:
            self.botoConfig = botocore.config.Config(read_timeout=900)
            self.botoClient = boto3.client('lambda',config=self.botoConfig)

        # wrap vertex types as patterns
        # wrap edge types as patterns
        # wrap shape types

    def trace(self,message):
        if self.args.trace:
            print message.encode('utf-8')

    def run(self,theLibrary):
        if self.args.threads:
            self.rlock = threading.RLock()
            for threadIndex in xrange(0,int(self.args.threads)):
                thread = threading.Thread(target=Generator._thread,args=[self,theLibrary])
                thread.start()
                self.threads.append(thread)
        else:
            self._thread(theLibrary)


    def _getNextIndex(self):
        if self.args.threads:
            self.rlock.acquire()
        done = self.resultIndex >= int(self.args.count)
        if not done:
            index = self.resultIndex
            self.resultIndex += 1
        if self.args.threads:
            self.rlock.release()
        if done:
            return None
        return index

    def _thread(self,theLibrary):
        index = self._getNextIndex()
        while index != None:
            success = False
            tries = 0
            while tries < 10 and not success:
                try:
                    self._try(theLibrary,index)
                    success = True
                except Exception as e:
                    print 'SOLVING FAILED: %s' % str(e)
                    traceback.print_exc()
                    tries += 1
            index = self._getNextIndex()


    def _try(self,theLibrary,index):
        if self.args.awsLambda:
            print 'BEGIN LAMBDA %d' % index
            response = self.botoClient.invoke(FunctionName=self.args.awsLambda,Payload=json.dumps({'modelKind':self.args.modelKind,
            'modelType':self.args.modelType}))
            asText = response.get('Payload').read()
            data = json.loads(json.loads(asText).get('body'))
            print 'END   LAMBDA %d' % index
        else:
            print 'BEGIN LOCAL %d' % index
            theTarget = target.Target(theLibrary,self.retained)
            theTarget.seed()
            if self.args.trace:
                theTarget.setTraceMode()
            theTarget.makeEntity(self.args.modelType,self.args.modelType)
            theTarget.close()
            #cProfile.runctx('theTarget.close()',globals(),locals())
            try:
                data = theTarget.toDict(True)
            except Exception as e:
                self.trace('ERROR: SOLVING FAILED:\n%s' % str(e))
                return False
            print 'END   LOCAL %d' % index

        fileName = '%s-%s-%s-%d.raw.json' % (self.args.modelKind,self.args.modelType,self.timestamp,index)
        print 'Writing Raw JSON output to %s' % fileName
        fp = open(fileName,'wt')
        if not fp:
            raise RuntimeError('Cannot write file  %s' % fileName)

        fp.write(json.dumps(data,indent=2))
        fp.close()
        return True

    def finish(self):
        for thread in self.threads:
            thread.join()

if __name__ == '__main__':
    generator = Generator()
    theLibrary = library.Library()
    theLibrary.loadPython(libPath)
    generator.run(theLibrary)
    generator.finish()

def lambda_handler(event, context):
    retained = dict()
    theLibrary = library.Library()
    theLibrary.loadPython(scriptPath)

    if not event.get('modelKind'):
        return {'statusCode': 500, 'body': json.dumps('Must provide arguments modelKind and modelType') }
    elif not event.get('modelType'):
        return {'statusCode': 500, 'body': json.dumps('Must provide arguments modelKind and modelType') }

    theTarget = target.Target(theLibrary,retained)
    theTarget.seed()
    if event.get('trace'):
        theTarget.setTraceMode(True)
    theTarget.makeEntity(event.get('modelKind'),event.get('modelType'))
    theTarget.close()
    try:
        data = theTarget.toDict(True)
    except Exception as e:
        return {'statusCode': 500, 'body': json.dumps('ERROR: SOLVING FAILED:\n%s' % str(e)) }

    return {
        'statusCode': 200,
        'body': json.dumps(data)
    }
