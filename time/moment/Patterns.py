class LeapMonthDay:
    def __str__(self):
      return 'leap_month_day'

    def project(self,constraint,args,target):
        yearIndex   = constraint.getArgument('yearIndex',target)
        monthIndex  = constraint.getArgument('monthIndex',target)
        mday        = constraint.getArgument('mday',target)

        mayLeapYearTrue = False
        mayLeapYearFalse = False
        if yearIndex.isUniversal() or not yearIndex.hasFewerValues(target.reductionThreshold):
            mayLeapYearTrue = True
            mayLeapYearFalse = True
        else:
            for year in yearIndex.multiple():
                if year % 4 == 0:
                    mayLeapYearTrue = True
                else:
                    mayLeapYearFalse = True
                if mayLeapYearTrue and mayLeapYearFalse:
                    break

        canLeapFalse = False
        if monthIndex.isUniversal():
            canLeapFalse = True
        elif mday.isUniversal():
            canLeapFalse = True
        else:
            for value in monthIndex.multiple():
                if value != 2:
                    canLeapFalse = True
                    break
            for value in mday.multiple():
                if value < 29:
                    canLeapFalse= True
                    break

        canMday29 = False
        canMday30 = False
        canMday31 = False
        if monthIndex.isUniversal():
            canMday29 = True
            canMday30 = True
            canMday31 = True
        else:
            for month in monthIndex.multiple():
                if month in (1,3,5,8,10,12):
                    canMday29 = True
                    canMday30 = True
                    canMday31 = True
                elif month == 2:
                    canMday29 = canMday29 or mayLeapYearTrue
                else:
                    canMday29 = True
                    canMday30 = True
                if canMday29 and canMday30 and canMday31:
                    break

        canMonthFeb = False
        canMonthShort = False
        canMonthLong = False
        if mday.isUniversal():
            canMonthFeb = True
            canMonthShort = True
            canMonthLong = True
        else:
            for value in mday.multiple():
                if value >= 1 and value <= 28:
                    canMonthFeb = True
                    canMonthShort = True
                    canMonthLong = True
                elif value <= 29:
                    canMonthFeb = mayLeapYearTrue
                    canMonthShort = True
                    canMonthLong = True
                elif value <= 30:
                    canMonthShort = True
                    canMonthLong = True
                else:
                    canMonthLong = True
                if canMonthFeb and canMonthShort and canMonthLong:
                    break

        reduceCount = 0
        if not yearIndex.isUniversal() and not canLeapFalse:
            valueList = [ year for year in yearIndex.multiple() if (year % 4 == 0) ]
            constraint.setArgument('yearIndex',target,packages.values.Values(valueList))
            reduceCount += 1

        if not canMonthFeb or not canMonthShort or not canMonthLong:
            valueList = []
            for value in xrange(1,13):
                if value in (1,3,5,8,10,12):
                    if canMonthLong:
                        valueList.append(value)
                elif value == 2:
                    if canMonthFeb:
                        valueList.append(value)
                else:
                    if canMonthShort:
                        valueList.append(value)
            constraint.setArgument('monthIndex',target,packages.values.Values(valueList))
            reduceCount += 1

        if not canMday29 or not canMday30 or not canMday31:
            valueList = range(1,29)
            if canMday29:
                valueList.append(29)
            if canMday30:
                valueList.append(30)
            if canMday31:
                valueList.append(31)
            constraint.setArgument('mday',target,packages.values.Values(valueList))
            reduceCount += 1

        if reduceCount > 0:
            return packages.constraint.REDUCE
        else:
            return packages.constraint.CONTINUE


class IsoDateFormatter:
    def __str__(self):
      return 'iso_date_formatter'

    def project(self,constraint,args,target):
        yearIndex   = constraint.getArgument('yearIndex',target)
        monthIndex  = constraint.getArgument('monthIndex',target)
        mday        = constraint.getArgument('mday',target)
        zoneOffset  = constraint.getArgument('zoneOffset',target)
        hour        = constraint.getArgument('hour',target)
        minute      = constraint.getArgument('minute',target)
        second      = constraint.getArgument('second',target)
        iso         = constraint.getArgument('iso',target)

        reduceCount = 0
        if 'iso' in args:
            if iso.isSingle():
                #dt = packages.dateutil.parser.parse(iso.single())
                arrow = packages.arrow.get(iso.single())
                #print 'DEBUG: IsoDateFormatter has %s' % arrow
                dt = arrow.datetime
                constraint.setArgument('yearIndex',target,packages.values.Values([dt.year]))
                constraint.setArgument('monthIndex',target,packages.values.Values([dt.month]))
                constraint.setArgument('mday',target,packages.values.Values([dt.day]))
                constraint.setArgument('hour',target,packages.values.Values([dt.hour]))
                constraint.setArgument('minute',target,packages.values.Values([dt.minute]))
                constraint.setArgument('second',target,packages.values.Values([dt.second]))

                if packages.re.search(r'[+-]\d\d:\d\d$',iso.single()):
                    offset = int(arrow.utcoffset().total_seconds() / 3600)
                else:
                    offset = None
                constraint.setArgument('zoneOffset',target,packages.values.Values([offset]))
                #target.setProperty(self,binding.get('zone').get('offset'),packages.values.Values([offset]))
                reduceCount += 1

        if not yearIndex.isSingle():
            target.trace('DEBUG: yearIndex is not single')
        elif not monthIndex.isSingle():
            target.trace('DEBUG: monthIndex is not single')
        elif not mday.isSingle():
            target.trace('DEBUG: mday is not single')
        elif not zoneOffset.isSingle():
            target.trace('DEBUG: zoneOffset is not single')
        elif not hour.isSingle():
            target.trace('DEBUG: hour is not single')
        elif not minute.isSingle():
            target.trace('DEBUG: minute is not single')
        elif not second.isSingle():
            target.trace('DEBUG: second is not single')
        else:
            #tz = packages.dateutil.tz.tzoffset(zone.single().get('abbrev'), 3600 * zone.single().get('offset'))
            dt = packages.datetime.datetime(yearIndex.single(),monthIndex.single(),mday.single(),hour.single(),minute.single(),second.single(),0)
            printed = dt.strftime('%Y-%m-%dT%H:%M:%S')
            if zoneOffset.single() != None:
                if zoneOffset.single() >= 0:
                    printed += '+%0.2d:00' % zoneOffset.single()
                else:
                    printed += '%0.2d:00' % zoneOffset.single()
            constraint.setArgument('iso',target,packages.values.Values([printed]))
            reduceCount += 1
        if reduceCount > 0:
            return packages.constraint.REDUCE
        else:
            return packages.constraint.CONTINUE

class Moment(packages.patterns.Object):
    global IsoDateFormatter
    global LeapMonthDay
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'year':   { 'name': 'year', 'semantic': 'time.moment.year' },
            'month':  { 'name': 'month', 'semantic': 'time.moment.month' },
            'mday':   { 'name': 'mday', 'semantic': 'time.moment.mday' },
            'hour':   { 'name': 'hour', 'semantic': 'time.moment.hour' },
            'minute': { 'name': 'minute', 'semantic': 'time.moment.minute' },
            'second': { 'name': 'second', 'semantic': 'time.moment.second' },
            'zone':   { 'name': 'zone', 'semantic': 'time.moment.zone' },
            'iso':    { 'encoding': { 'java': 'java.lang.String' } }
        })

    def fromValue(self,who,binding,value):
        if isinstance(value,packages.datetime.datetime):
            dt = value
            target = binding.getTarget()
            target.setProperty(who,binding.get('year').get('index'),value.year)
            target.setProperty(who,binding.get('month').get('@value').get('index'),value.month)
            target.setProperty(who,binding.get('mday'),value.day)
            target.setProperty(who,binding.get('hour'),value.hour)
            target.setProperty(who,binding.get('minute'),value.minute)
            target.setProperty(who,binding.get('second'),value.second)
            return
        raise RuntimeError('Cannot load %s from %s' % (type(self),type(value)))

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.constrain(IsoDateFormatter(),dict(
            yearIndex=binding.rootPath.get('year').get('index'),
            monthIndex=binding.rootPath.get('month').get('@value').get('index'),
            mday=binding.rootPath.get('mday'),
            hour=binding.rootPath.get('hour'),
            minute=binding.rootPath.get('minute'),
            second=binding.rootPath.get('second'),
            zoneOffset=binding.rootPath.get('zone').get('@value').get('offset'),
            iso=binding.rootPath.get('iso')
        ))
        target.constrain(LeapMonthDay(),dict(
            yearIndex=binding.rootPath.get('year').get('index'),
            monthIndex=binding.rootPath.get('month').get('@value').get('index'),
            mday=binding.rootPath.get('mday')
        ))

exports = [Moment]
