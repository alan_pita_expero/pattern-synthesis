class PastYear100(packages.patterns.Object):
    def __init__(self):
        max = packages.datetime.datetime.now().year
        min = max - 100
        packages.patterns.Object.__init__(self,scriptPackage,{
            'index':  { 'values': xrange(min,max+1) },
        })

class PastYear10(packages.patterns.Object):
    def __init__(self):
        max = packages.datetime.date.today().year
        min = max - 10
        packages.patterns.Object.__init__(self,scriptPackage,{
            'index':  { 'values': xrange(min,max+1) },
        })

class LastYear(packages.patterns.Object):
    def __init__(self):
        min = packages.datetime.datetime.now().year - 1
        max = min
        packages.patterns.Object.__init__(self,scriptPackage,{
            'index':  { 'values': xrange(min,max+1) },
        })

class Now(packages.patterns.Object):
    def __init__(self):
        min = packages.datetime.datetime.now().year
        max = min
        packages.patterns.Object.__init__(self,scriptPackage,{
            'index':  { 'values': xrange(min,max+1) },
        })

class NextYear(packages.patterns.Object):
    def __init__(self):
        min = packages.datetime.datetime.now().year + 1
        max = min
        packages.patterns.Object.__init__(self,scriptPackage,{
            'index':  { 'values': xrange(min,max+1) },
        })

class FutureYear10(packages.patterns.Object):
    def __init__(self):
        min = packages.datetime.datetime.now().year
        max = min + 10
        packages.patterns.Object.__init__(self,scriptPackage,{
            'index':  { 'values': xrange(min,max+1) },
        })

class FutureYear100(packages.patterns.Object):
    def __init__(self):
        min = packages.datetime.datetime.now().year
        max = min + 100
        packages.patterns.Object.__init__(self,scriptPackage,{
            'index':  { 'values': xrange(min,max+1) },
        })

#exports = [PastYear100,PastYear10,LastYear,Now,NextYear,FutureYear10,FutureYear100]
exports = [Now]
