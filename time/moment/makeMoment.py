import os
import sys

scriptPath = os.path.dirname(os.path.realpath(__file__))
sys.path.append('%s/../..' % scriptPath)
import target

target = target.Target()
target.makeEntity('moment','time.moment')
target.close()
print unicode(target.toDict()).encode('utf-8')
