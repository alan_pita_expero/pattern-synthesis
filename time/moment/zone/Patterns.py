# source: https://github.com/dmfilipenko/timezones.json.git

fp = open('%s/timezones.json' % (scriptPath))
zones = packages.json.load(fp)

localTimeZone = packages.json.loads("""{
    "value": "Local",
    "abbr": "LOC",
    "offset": null,
    "isdst": false,
    "text": "Local Time (zone unspecified)",
    "utc": null
}""")

db = []
for zone in zones:
    db.append(zone.copy())
#    for utcValue in zone.get('utc'):
#        tmp = zone.copy()
#        tmp['utc'] = utcValue
#        db.append(tmp)
db.append(localTimeZone)

class AnyTimezone(packages.patterns.Trait):
    global db

    def __init__(self):
        packages.patterns.Trait.__init__(self,scriptPackage,db,{
            "value": { "java": "java.lang.String" },
            "abbr": { "java": "java.lang.String" },
            "offset":{ "java": "java.lang.String" },
            "isdst": { "java": "java.lang.Boolean" },
            "text": { "java": "java.lang.String" },
            "utc": { "java": "java.lang.String" }
        })

exports = [AnyTimezone]
