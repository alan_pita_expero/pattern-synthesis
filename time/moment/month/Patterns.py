months = [
  { "index": 1,   "en": { "full": "January", "abbrev": "Jan" }},
  { "index": 2,   "en": { "full": "February", "abbrev": "Feb" }},
  { "index": 3,   "en": { "full": "March", "abbrev": "Mar" }},
  { "index": 4,   "en": { "full": "April", "abbrev": "Apr" }},
  { "index": 5,   "en": { "full": "May", "abbrev": "May" }},
  { "index": 6,   "en": { "full": "June", "abbrev": "Jun" }},
  { "index": 7,   "en": { "full": "July", "abbrev": "Jul" }},
  { "index": 8,   "en": { "full": "August", "abbrev": "Aug" }},
  { "index": 9,   "en": { "full": "September", "abbrev": "Sep" }},
  { "index": 10,   "en": { "full": "October", "abbrev": "Oct" }},
  { "index": 11,  "en": { "full": "November", "abbrev": "Nov" }},
  { "index": 12,  "en": { "full": "December", "abbrev": "Dec" }},
]

class Moment(packages.patterns.Trait):
    global months

    def __init__(self):
        packages.patterns.Trait.__init__(self,scriptPackage,months,{
            'index': { 'encoding': { 'java': 'java.lang.Integer' } },
            'en.full': { 'encoding': { 'java': 'java.lang.String' } },
            'en.abbrev': { 'encoding': { 'java': 'java.lang.String' } }
        })

exports = [Moment]
