class PickDuration:
    def __str__(self):
        return 'pick_duration'

    def suggestDurationSeconds(self):
        valueList = []

        # duration measured in seconds
        valueList.append(1)
        valueList.append(10)

        # duration measured in minutes
        valueList.append(60)
        valueList.append(60*5)
        valueList.append(60*10)
        valueList.append(60*15)
        valueList.append(60*30)
        valueList.append(60*45)
        valueList.append(60*60)

        # duration measured in hours
        #for hours in xrange(1,25):
        #    valueList.append(60*60*hours)

        # duration measured in days
        #for days in xrange(1,31):
        #    valueList.append(60*60*24*days)

        # duration measured in months (approx)
        #for months in xrange(1,25):
        #    valueList.append(60*60*24*30*months)

        # duration measured in years (approx)
        #for years in xrange(1,25):
        #    valueList.append(60*60*24*365*years)
        return packages.values.Values(valueList)

    def project(self,constraint,args,target):
        startYear     = constraint.getArgument('startYear',target)
        startMonth    = constraint.getArgument('startMonth',target)
        startDay      = constraint.getArgument('startDay',target)
        startHour     = constraint.getArgument('startHour',target)
        startMinute   = constraint.getArgument('startMinute',target)
        startSecond   = constraint.getArgument('startSecond',target)

        endYear       = constraint.getArgument('endYear',target)
        endMonth      = constraint.getArgument('endMonth',target)
        endDay        = constraint.getArgument('endDay',target)
        endHour       = constraint.getArgument('endHour',target)
        endMinute     = constraint.getArgument('endMinute',target)
        endSecond     = constraint.getArgument('endSecond',target)

        seconds       = constraint.getArgument('seconds',target)

        if not seconds.isSingle():
            variable = constraint.getVariable('seconds',target)
            target.trace('pick_duration reduces %s' % variable)
            if seconds.isUniversal():
                constraint.setArgument('seconds',target,self.suggestDurationSeconds())
            else:
                constraint.setArgument('seconds',target,seconds.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not startSecond.isSingle():
            variable = constraint.getVariable('startSecond',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('startSecond',target,startSecond.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not endSecond.isSingle():
            variable = constraint.getVariable('endSecond',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('endSecond',target,endSecond.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not startMinute.isSingle():
            variable = constraint.getVariable('startMinute',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('startMinute',target,startMinute.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not endMinute.isSingle():
            variable = constraint.getVariable('endMinute',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('endMinute',target,endMinute.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not startHour.isSingle():
            variable = constraint.getVariable('startHour',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('startHour',target,startHour.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not endHour.isSingle():
            variable = constraint.getVariable('endHour',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('endHour',target,endHour.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not startDay.isSingle():
            variable = constraint.getVariable('startDay',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('startDay',target,startDay.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not endDay.isSingle():
            variable = constraint.getVariable('endDay',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('endDay',target,endDay.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not startMonth.isSingle():
            variable = constraint.getVariable('startMonth',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('startMonth',target,startMonth.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not endMonth.isSingle():
            variable = constraint.getVariable('endMonth',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('endMonth',target,endMonth.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not startYear.isSingle():
            variable = constraint.getVariable('startYear',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('startYear',target,startYear.reduce(variable,target))
            return packages.constraint.AGAIN
        elif not endYear.isSingle():
            variable = constraint.getVariable('endYear',target)
            target.trace('pick_duration reduces %s' % variable)
            constraint.setArgument('endYear',target,endYear.reduce(variable,target))
            return packages.constraint.AGAIN
        else:
            target.trace('pick_duration reduces none')
            return packages.constraint.CONTINUE

class CalcDuration:
    def __str__(self):
        return 'calc_duration'

    def clipForward(self,constraint,target,startArg,startName,endArg,endName,grains,carryValues):
        nextCarryValues = None
        if not startArg.isUniversal():
            nextCarryValues = []
            nextValues = []
            for grain in grains or [None]:
                for startValue in startArg.multiple():
                    for carryValue in carryValues:
                        sum = startValue + carryValue
                        if grain:
                            nextCarry = sum / grain
                            nextValue = sum % grain
                        else:
                            nextCarry = 0
                            nextValue = sum
                        if nextCarry not in nextCarryValues:
                            nextCarryValues.append(nextCarry)
                        if nextValue not in nextValues:
                            nextValues.append(nextValue)
            target.trace('clipForward computes %s %s %s' % (endName,nextValues,nextCarryValues))
            constraint.setArgument(endName,target,packages.values.Values(nextValues))
        return nextCarryValues

    def clipBackward(self,constraint,target,startArg,startName,endArg,endName,grains,carryValues):
        nextCarryValues = None
        if not endArg.isUniversal():
            nextCarryValues = []
            nextValues = []
            for grain in grains or [None]:
                for endValue in endArg.multiple():
                    for carryValue in carryValues:
                        diff = endValue - carryValue
                        if grain:
                            nextCarry = abs(diff / grain)
                            nextValue = diff % grain
                        else:
                            nextCarry = 0
                            nextValue = diff
                        if nextCarry not in nextCarryValues:
                            nextCarryValues.append(nextCarry)
                        if nextValue not in nextValues:
                            nextValues.append(nextValue)
            target.trace('clipBackward computes %s %s %s' % (startName,nextValues,nextCarryValues))
            constraint.setArgument(startName,target,packages.values.Values(nextValues))
        return nextCarryValues

    def makeAllCarryValues(self,carryValues,grains):
        nextCarryValues = []
        for grain in grains:
            for carryValue in carryValues:
                nextCarryValue = carryValue / grain
                if nextCarryValue not in nextCarryValues:
                    nextCarryValues.append(nextCarryValue)
                nextCarryValue = 1 + (carryValue / grain)
                if nextCarryValue not in nextCarryValues:
                    nextCarryValues.append(nextCarryValue)
        return nextCarryValues

    def clip(self,constraint,target,startArg,startName,endArg,endName,grain,carryValues):
        nextCarryValues = None
        target.trace('CalcDuration.clip startName=%s endName=%s carryValues=%s' % (startName,endName,carryValues))
        if grain in ('second','minute'):
            nextCarryValues = self.makeAllCarryValues(carryValues,[60])
            nextCarryValues = self.clipForward(constraint,target,startArg,startName,endArg,endName,[60],carryValues) or nextCarryValues
            nextCarryValues = self.clipBackward(constraint,target,startArg,startName,endArg,endName,[60],carryValues) or nextCarryValues
        elif grain == 'hour':
            nextCarryValues = self.makeAllCarryValues(carryValues,[24])
            nextCarryValues = self.clipForward(constraint,target,startArg,startName,endArg,endName,[24],carryValues) or nextCarryValues
            nextCarryValues = self.clipBackward(constraint,target,startArg,startName,endArg,endName,[24],carryValues) or nextCarryValues
        elif grain == 'day':
            daysPerMonth = [28,29,30,31]
            if not constraint.getArgument('startMonth',target).isUniversal():
                daysPerMonth = []
                for month in constraint.getArgument('startMonth',target).multiple():
                    if month in (1,3,5,7,8,10,12) and 31 not in daysPerMonth:
                        daysPerMonth.append(31)
                    elif month in (4,6,9,11) and 30 not in daysPerMonth:
                        daysPerMonth.append(30)
                    elif month == 2:
                        if 28 not in daysPerMonth:
                            daysPerMonth.append(28)
                        if 29 not in daysPerMonth:
                            daysPerMonth.append(29)

            nextCarryValues = self.makeAllCarryValues(carryValues,daysPerMonth)
            nextCarryValues = self.clipForward(constraint,target,startArg,startName,endArg,endName,daysPerMonth,carryValues) or nextCarryValues
            nextCarryValues = self.clipBackward(constraint,target,startArg,startName,endArg,endName,daysPerMonth,carryValues) or nextCarryValues
        elif grain == 'month':
            nextCarryValues = self.makeAllCarryValues(carryValues,[12])
            nextCarryValues = self.clipForward(constraint,target,startArg,startName,endArg,endName,[12],carryValues) or nextCarryValues
            nextCarryValues = self.clipBackward(constraint,target,startArg,startName,endArg,endName,[12],carryValues) or nextCarryValues
        else:
            nextCarryValues = None
            nextCarryValues = self.clipForward(constraint,target,startArg,startName,endArg,endName,None,carryValues) or nextCarryValues
            nextCarryValues = self.clipBackward(constraint,target,startArg,startName,endArg,endName,None,carryValues) or nextCarryValues

        return nextCarryValues

    def project(self,constraint,args,target):
        startYear     = constraint.getArgument('startYear',target)
        startMonth    = constraint.getArgument('startMonth',target)
        startDay      = constraint.getArgument('startDay',target)
        startHour     = constraint.getArgument('startHour',target)
        startMinute   = constraint.getArgument('startMinute',target)
        startSecond   = constraint.getArgument('startSecond',target)

        endYear       = constraint.getArgument('endYear',target)
        endMonth      = constraint.getArgument('endMonth',target)
        endDay        = constraint.getArgument('endDay',target)
        endHour       = constraint.getArgument('endHour',target)
        endMinute     = constraint.getArgument('endMinute',target)
        endSecond     = constraint.getArgument('endSecond',target)

        seconds       = constraint.getArgument('seconds',target)

        # reduce seconds
        if seconds.isSingle():
            carryValues = self.clip(constraint,target,startSecond,'startSecond',endSecond,'endSecond','second',[seconds.single()])
            carryValues = self.clip(constraint,target,startMinute,'startMinute',endMinute,'endMinute','minute',carryValues)
            carryValues = self.clip(constraint,target,startHour,  'startHour',  endHour,  'endHour',  'hour',  carryValues)
            carryValues = self.clip(constraint,target,startDay,   'startDay',   endDay,   'endDay',   'day',   carryValues)
            carryValues = self.clip(constraint,target,startMonth, 'startMonth', endMonth, 'endMonth', 'month', carryValues)
            carryValues = self.clip(constraint,target,startYear,  'startYear',  endYear,  'endYear',  'year',  carryValues)
            return packages.constraint.REDUCE
        return packages.constraint.CONTINUE

class Period(packages.patterns.Object):
    global CalcDuration,PickDuration
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'start': { 'name': 'start', 'semantic': 'time.moment' },
            'end': { 'name': 'end', 'semantic': 'time.moment' },
            #'years': { 'encoding': { 'java': 'java.lang.Integer' } },
            #'monthCarry': { 'values': [ 0 ] },
            #'months':  { 'encoding': { 'java': 'java.lang.Integer' } },
            #'dayCarry': { 'values': [ 0 ] },
            #'days':   { 'encoding': { 'java': 'java.lang.Integer' } },
            #'hourCarry': { 'values': [ 0 ] },
            #'hours':   { 'encoding': { 'java': 'java.lang.Integer' } },
            #'minuteCarry': { 'values': [ 0 ] },
            #'minutes': { 'encoding': { 'java': 'java.lang.Integer' } },
            #'secondCarry': { 'values': [ 0 ] },
            'seconds': { 'encoding': { 'java': 'java.lang.Integer' } }
        })

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)

        binding.get('start.zone').sync(binding.get('end.zone'))
        args = {
            'startYear'   : binding.get('start.year.index'),
            'startMonth'  : binding.get('start.month.@value.index'),
            'startDay'    : binding.get('start.mday'),
            'startHour'   : binding.get('start.hour'),
            'startMinute' : binding.get('start.minute'),
            'startSecond' : binding.get('start.second'),

            'endYear'     : binding.get('end.year.index'),
            'endMonth'    : binding.get('end.month.@value.index'),
            'endDay'      : binding.get('end.mday'),
            'endHour'     : binding.get('end.hour'),
            'endMinute'   : binding.get('end.minute'),
            'endSecond'   : binding.get('end.second'),

            #'years ' : binding.get('years'),
            #'months' : binding.get('months'),
            #'days'   : binding.get('days'),
            #'hours'  : binding.get('hours'),
            #'minutes': binding.get('minutes'),
            'seconds': binding.get('seconds')
        }
        target.constrain(CalcDuration(),args)
        target.constrain(PickDuration(),args,False)

class Hours(Period):
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'start': { 'name': 'start', 'semantic': 'time.moment' },
            'end': { 'name': 'end', 'semantic': 'time.moment' },
            'years': { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'monthCarry': { 'values': [ 0 ] },
            'months':  { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'dayCarry': { 'values': [ 0 ] },
            'days':   { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'hourCarry': { 'values': [ 0 ] },
            'hours':   { 'encoding': { 'java': 'java.lang.Integer' }, 'values': xrange(1,23) },
            'minuteCarry': { 'values': [ 0 ] },
            'minutes': { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'secondCarry': { 'values': [ 0 ] },
            'seconds': { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] }
        })

class Days(Period):
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'start': { 'name': 'start', 'semantic': 'time.moment' },
            'end': { 'name': 'end', 'semantic': 'time.moment' },
            'years': { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'monthCarry': { 'values': [ 0 ] },
            'months':  { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'dayCarry': { 'values': [ 0 ] },
            'days':   { 'encoding': { 'java': 'java.lang.Integer' }, 'values': xrange(0,31) },
            'hourCarry': { 'values': [ 0 ] },
            'hours':   { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'minuteCarry': { 'values': [ 0 ] },
            'minutes': { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'secondCarry': { 'values': [ 0 ] },
            'seconds': { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] }
        })

class Months(Period):
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'start': { 'name': 'start', 'semantic': 'time.moment' },
            'end': { 'name': 'end', 'semantic': 'time.moment' },
            'years': { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'monthCarry': { 'values': [ 0 ] },
            'months':  { 'encoding': { 'java': 'java.lang.Integer' }, 'values': xrange(1,12) },
            'dayCarry': { 'values': [ 0 ] },
            'days':   { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'hourCarry': { 'values': [ 0 ] },
            'hours':   { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'minuteCarry': { 'values': [ 0 ] },
            'minutes': { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] },
            'secondCarry': { 'values': [ 0 ] },
            'seconds': { 'encoding': { 'java': 'java.lang.Integer' }, 'values': [0] }
        })

exports = [Period]
