# -*- coding: utf-8 -*-
import json

fp = open('%sus-phone-area-codes.json' % (scriptPath))
areaCodes = json.load(fp)

class UsPhoneNumberFormatter:
    def project(self,constraint,args,target):
        usAreaCode   = constraint.getArgument('usAreaCode',target)
        usPrefix     = constraint.getArgument('usPrefix',target)
        usSubscriber = constraint.getArgument('usSubscriber',target)
        us7digit     = constraint.getArgument('us7digit',target)
        us10digit    = constraint.getArgument('us10digit',target)
        intl         = constraint.getArgument('intl',target)

        result = packages.constraint.CONTINUE
        if 'usPrefix' in args or 'usSubscriber' in args:
            if usPrefix.isSingle() and usSubscriber.isSingle():
                value = """%s-%s""" % (usPrefix.single(),usSubscriber.single())
                constraint.setArgument("us7digit",target,value)
                result = packages.constraint.REDUCE

        if 'usAreaCode' in args or 'usPrefix' in args or 'usSubscriber' in args:
            if usAreaCode.isSingle() and usPrefix.isSingle() and usSubscriber.isSingle():
                value = """(%s)%s-%s""" % (usAreaCode.single().get('code'),usPrefix.single(),usSubscriber.single())
                constraint.setArgument("us10digit",target,value)
                value = """+1 (%s)%s-%s""" % (usAreaCode.single().get('code'),usPrefix.single(),usSubscriber.single())
                constraint.setArgument("intl",target,value)
                result = packages.constraint.REDUCE
        return result

class USPhoneNumber(packages.patterns.Object):
    global UsPhoneNumberFormatter
    global areaCodes
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'id': { 'name': 'id', 'semantic': 'datacube.vertex.id' },
            'usAreaCode': { 'values': areaCodes },
            'usPrefix': { 'values': xrange(200,999) },
            'usSubscriber': { 'values': xrange(1000,9999) },
            'us7digit': { 'encoding': { 'java': 'java.lang.String' } },
            'us10digit': { 'encoding': { 'java': 'java.lang.String' } },
            'intl': { 'encoding': { 'java': 'java.lang.String' } }
        })

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.setProperty(self,properties.get('id').get('label'),['Phone'])
        target.constrain(UsPhoneNumberFormatter(),dict(
            usAreaCode=binding.rootPath.get('usAreaCode'),
            usPrefix=binding.rootPath.get('usPrefix'),
            usSubscriber=binding.rootPath.get('usSubscriber'),
            us7digit=binding.rootPath.get('us7digit'),
            us10digit=binding.rootPath.get('us10digit'),
            intl=binding.rootPath.get('intl')
        ))

exports = [USPhoneNumber]
