import random
#import xeger

class Contact(packages.patterns.BasePattern):
    def __init__(self):
        packages.patterns.BasePattern.__init__(self,scriptPackage)

    def canEnumerate(self,count):
        return False

    def doEnumerate(self,count):
        raise RuntimeError('Unsupported Method')

    def symbol():
        return random.choice(['!','@','$','%','^','&','(',')','[',']',':',';','.',','])

    def jibberish():
        length = random.choice(xrange(0,3))
        return [ symbol() for i in xrange(0,length) ]

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.BasePattern.injectInto(self,binding)
        if target.hasProperty(properties.get('email')):
            lorem = target.injectEntities(properties,'lorem.phrase')
            words = []
            if fullNameProp:
                words = fullNameProp.split(' ')
            elif familyNameProp and givenNameProp:
                words = [familyNameProp,givenNameProp]
            elif familyNameProp:
                words = familyNameProp
            elif givenNameProp:
                words = givenNameProp
            else:
                words = lorem.random()

            username = jibberish()
            for i in xrange(0,len(words)):
                username += words[i]
                username += jibberish()
            target.setProperty(self,properties.get('email'),username)

#        if properties.get('phone') and properties.get('phone.number'):
#            gen = xeger.Xeger(limit=256)
#            value = gen.xeger("[0-9]{3}-[0-9]{3}-[0-9]{4}")
#            target.setProperty(self,properties.get('phone.number'),username)

exports = [Contact]
