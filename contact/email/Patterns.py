import random

class EmailFormatter:
    def append(self,mode,word,sep):
        value = None
        if not mode.isSingle():
            pass
        elif mode.single() == 'drop':
            value = ''
        elif not word.isSingle():
            pass
        elif mode.single() == 'lower':
            value = word.single().lower()
        elif mode.single() == 'upper':
            value = word.single().upper()
        elif mode.single() == 'caps':
            value = word.single()[0].upper()
            value += word.single()[1:].lower()
        if value == None:
            return None
        if sep == None:
            pass
        elif not sep.isSingle():
            value = None
        else:
            value += sep.single()
        return value

    def project(self,constraint,args,target):
        username  = constraint.getArgument('username',target)
        host  = constraint.getArgument('host',target)

        if args == ['value']:
            target.trace('SKIP - changed arg is %s' % arg)
        else:
            value = ''
            if not username.isSingle():
                return packages.constraint.CONTINUE
            elif not host.isSingle():
                return packages.constraint.CONTINUE
            value = '%s@%s' % (username.single(),host.single())
            constraint.setArgument('value',target,packages.values.Values([value]))
            return packages.constraint.REDUCE
        return packages.constraint.CONTINUE

class Email(packages.patterns.Object):
    global EmailFormatter
    def __init__(self):
        packages.patterns.Object.__init__(self,scriptPackage,{
            'id': { 'name': 'id', 'semantic': 'datacube.vertex.id' },
            'username': { 'name': 'username', 'semantic': 'computer.identity.username' },
            'host': { 'name': 'host', 'semantic': 'computer.server' },
            'value': { 'encoding': { 'java': 'java.lang.String' } }
        })

    def injectInto(self,binding):
        target = binding.getTarget()
        properties = binding.getRootPath()
        packages.patterns.Object.injectInto(self,binding)
        target.setProperty(self,properties.get('id').get('label'),['Email'])
        #print 'DEBUG: email has properties %s' % [ prop.name for prop in self.properties ]
        target.constrain(EmailFormatter(),dict(
            username=binding.rootPath.get('username').get('value'),
            host=binding.rootPath.get('host'),
            value=binding.rootPath.get('value')
        ))

exports = [Email]
