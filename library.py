#!/usr/bin/env python
import constraint
import datetime
import arrow
import pytz
import tzlocal
import random
import values
import patterns
import json
import xeger
import re
import os
import urllib
import requests

class Packages:
    def __init__(self):
        self.constraint=constraint
        self.datetime=datetime
        self.arrow=arrow
        self.pytz=pytz
        self.tzlocal=tzlocal
        self.random=random
        self.values=values
        self.patterns=patterns
        self.json = json
        self.xeger = xeger
        self.re = re
        self.os = os
        self.urllib = urllib
        self.requests = requests

packages = Packages()

class Library:
  global packages

  def __init__(self):
      self.packageToPattern = dict()
      self.packages = packages

  def addPattern(self,package,exports):
      self.packageToPattern[package] = exports

  def locate(self,package):
      return self.packageToPattern.get(package)

  def getPackageNames(self):
      return self.packageToPattern.keys()

  def loadPython(self,libPath):
      self.loadPython_r(libPath,[])

  def loadPython_r(self,libPath,subPath):
      package = '.'.join(subPath)
      dir = '/'.join([libPath]+subPath)
      prefix = dir + '/'

      patternsFile = prefix+'Patterns.py'
      if len(subPath) > 0 and os.path.isfile(patternsFile):
          print('Loading patterns from %s ...' % patternsFile)
          globals = dict(scriptPath=prefix,scriptPackage=package,packages=self.packages)
          locals  = dict()
          execfile(patternsFile,globals,locals)
          exports = globals.get('exports') or locals.get('exports')
          print("   %d patterns loaded from %s" % (len(exports),patternsFile))
          self.addPattern(package,[ klass() for klass in exports ])
      for sub in os.listdir(dir):
          if os.path.isdir(prefix+sub):
              self.loadPython_r(libPath,subPath+[sub])

  def makeMeta(self):
      meta = []
      # generate enum definitions
      for patternPath,patternClasses in self.packageToPattern.iteritems():
          thePattern = patternClasses[0]()
          if hasattr(thePattern,'makeMeta'):
              meta += thePattern.makeMeta(self,patternPath)
      return meta
